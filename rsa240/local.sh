build_base="${up_path}/build"
host_short="`hostname | cut -d. -f1 | sed -e 's/-[0-9]*$//'`"
build_tree="${build_base}/${host_short}"

# These are _examples_ of config variables that are recognized by
# cado-nfs to set up the tools it uses. Uncomment only if these tools are
# not found in your default path.

# GMP=$HOME/Packages/gmp-6.2.0/
# HWLOC=$HOME/Packages/hwloc-2.0.2/
# MPI=$HOME/Packages/openmpi-4.0.3/

# A common situation is when your compute environment uses the tcl
# "Modules" package, in which case it is appropriate to configure the
# right set of modules right from within local.sh. This might be done as
# follows. Again, uncomment and edit if this is appropriate for your
# setup.
# if ! type -p module && [ -f /etc/profile.d/modules.sh ] ; then
#     . /etc/profile.d/modules.sh
# fi
# module purge
# # # module use $HOME/.modules   # own user modules
# # maybe use a custom cmake
# module load build/cmake
# module load compiler/gcc
# module load compiler/intel
# module load mpi/intel


# We _must_ set MPI to at least something if we want an MPI build. A
# simple "MPI=1" picks mpicc and friends from the path.
: ${MPI=1}

: ${ENABLE_SHARED=0}
: ${CFLAGS="-O3 -DNDEBUG"}
: ${CXXFLAGS="$CFLAGS"}
