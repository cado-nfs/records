Recompile cado-nfs
==================

1. Increase the value of `NB_MAX_METHODS` in the `#define` in the
`sieve/ecm/facul.hpp` file. (600 should be enough)

2. Make sure that you have GMP-ECM is installed and your machine and
visible from cado-nfs's compilation toolchain.

You might have to add something like
```
       GMPECM=/path/to/gmpecm
```
in your local.sh in order to activate the compilation of
`misc/descent_init_Fp` which depends on GMP-ECM. Note that it might be
necessary to configure gmp-ecm with the `--enable-shared` option so that it
is recognized by cmake.

3. As usual for a large experiment, add the following in `local.sh`
```
       FLAGS_SIZE="-DSIZEOF_P_R_VALUES=8 -DSIZEOF_INDEX=8"
```

4) Compile...

Prepare a working directory
===========================

```
# Choose a place
wdir=/path/to/dlp240

# copy relevant stuff. You should have the following files :
dlp240.badidealinfo
dlp240.badideals
dlp240.poly
dlp240.reconstructlog.dlog
dlp240.renumber
dlp240.roots0.gz
dlp240.roots1.gz
dlp240.hint
```

Prepare the target
==================

In what follows, $target must be a shell variable containing the target
written in decimal. It can be for instance:

0. Harcoded target:
```
target="123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100"
```

1. Target taken from a sentence:
```
target_str="The magic words are still Squeamish Ossifrage"
target_hex=`echo -n $target_str | xxd -p -c 256`
target_hex=${target_hex^^}
target=`echo "ibase=16; $target_hex" | BC_LINE_LENGTH=0 bc`
```

2. Target taken from a formula computed with Magma / Sage:
```
target=`echo "Ceiling(2^793*Pi(RealField(250)));" | magma -b`
```

Set the short name for the target:
==================================

The descent.py script uses a short name for the target. Since we are cheating
with this, we need to use this internal name:
```
short_target="${target:0:10}...${target: -10}"
```

Initialize the descent:
=======================

```
# choose the path to your CADO_NFS build directory
CADO_BUILD=/path/to/cado-nfs/build

p=124620366781718784065835044608106590434820374651678805754818788883289666801188210855036039570272508747509864768438458621054865537970253930571891217684318286362846948405301614416430468066875699415246993185704183030512549594371372159029285303
ell=62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651

# run as many as you want of the following in parallel, changing the seed for
# each run:
$CADO_BUILD/misc/descent_init_Fp -poly $wdir/dlp240.poly -mt 36 -minB1 2000 -mineff 2000 -maxeff 5000000 -side 1 -extdeg 1 -lpb 100 -seed 42 -jl $p $target

# take the output of a winner and put it in a file. This should look like this:
cat > $wdir/dlp240.descent.${short_target}.upper.init << EOF
Candidate id = 585233622
u0=60367878646407689470146901037998662561396457610337478483729105939030015431877417984175014476091445958717106736403214645587585
v0=84075603946788417030863736673970464822468061004922396172066836932653426055145362881239957001348521808236073651447496856289409
u=1 (1 bits) largest prime so far of 83 bits
v=1 (1 bits) largest prime so far of 85 bits
effort=7954287
U = 11115814654987621436651726313,84293370936324477859384006099,101013631005385213436119240940,-401910637140654498305458570967
V = -140220538264790317274834216493,153214328933512769640372578392,-10489210686953161453362359657,-624779326557421402049330473855
u = 60367878646407689470146901037998662561396457610337478483729105939030015431877417984175014476091445958717106736403214645587585
v = 84075603946788417030863736673970464822468061004922396172066836932653426055145362881239957001348521808236073651447496856289409
fac_u = 3,0 5,2 113,105 1543,356 910277,243015 1910560429,37831370 279110168690986697,116587196982800125 31159723474838178763,24324908549573488953 93860444700928412669,43109805353414622678 2600544447331951089877,708175219608964760816 6252011894338296607929059,4661104531847904984562439
fac_v = 3,0 7,6 41,22 113,105 401,246 24184471581643,22596272999833 176604796267457,26557178864616 6561645365183358887,635986908120345731 6544290316035145782743,3455437384487602992657 504792458677175692158691,465950935946437207062580 23276450899396385546688373,1872137450262637490928223
Youpi: e = 585233622 is a winner
Total CPU time: 453555.0 s
EOF
```

Create the todo file
====================
```
cd $wdir
cat <<EOF | magma -b
Init := Split(Read("dlp240.descent.${short_target}.upper.init"));
Out := [];
for line in Init do
  l := Split(line, " ");
  if l[1] ne "fac_u" and l[1] ne "fac_v" then continue; end if;
  for i := 3 to #l do
    q,r := Explode([ StringToInteger(x) : x in Split(l[i], ",")]);
    if q ge 2^20 then
      Append(~Out, [1,q,r]);
    end if;
  end for;
end for;

for x in Out do
  fprintf "dlp240.descent.${short_target}.upper.todo", "%o %o %o\n", x[1],x[2],x[3];
end for;
EOF
```

Run las_descent manually
========================
```
$CADO_BUILD/sieve/las_descent --renumber $wdir/dlp240.renumber --log $wdir/dlp240.reconstructlog.dlog --recursive-descent --allow-largesq --never-discard --fb1 $wdir/dlp240.roots1.gz --poly $wdir/dlp240.poly --fb0 $wdir/dlp240.roots0.gz --descent-hint-table $wdir/dlp240.hint --I 16 --lim0 1073741824 --lim1 1073741824 --lpb0 35 --mfb0 105 --lpb1 35 --mfb1 105 -t 16 --todo $wdir/dlp240.descent.${short_target}.upper.todo -v -bkmult 1,1s:1.09123 > $wdir/dlp240.descent.${short_target}.middle.rels 2>&1
```

Extract all the known logs of the ideals involved in the descent trees and create a fake .dlog file
===================================================================================================

```
cd $wdir
tempfile1=`mktemp`
tempfile2=`mktemp`
tempfile3=`mktemp`

grep "Taken" dlp240.descent.${short_target}.middle.rels | cut -d " " -f 2 > $tempfile1

cat <<EOF | magma -b
Rels := Split(Read("$tempfile1"));
Out := [];
for line in Rels do
  l := Split(line, ":");
  a, b := Explode([ StringToInteger(x) : x in Split(l[1], ",")]);
  for side in [0,1] do
    listq := {StringToInteger(x, 16) : x in Split(l[side+2], ",")};
    for q in listq do
      if q ge 2^16 and q le 2^35 then
        r := Integers()!(GF(q)!a/b);
        Append(~Out, [side,q,r]);
      end if;
    end for;
  end for;
end for;

Init := Split(Read("dlp240.descent.${short_target}.upper.init"));
for line in Init do
  l := Split(line, " ");
  if l[1] ne "fac_u" and l[1] ne "fac_v" then continue; end if;
  for i := 3 to #l do
    q,r := Explode([ StringToInteger(x) : x in Split(l[i], ",")]);
    if q ge 2^16 and q le 2^35 then
      Append(~Out, [1,q,r]);
    end if;
  end for;
end for;

Out := Sort(Out, func<x, y | x[2] - y[2]>);

for x in Out do
  fprintf "$tempfile2", "%o,%o,%o\n", x[1],x[2],x[3];
end for;
EOF

// Now, use the  seek_in_dlogfile.c binary

gcc -O2 seek_in_dlogfile.c -lgmp
cat $tempfile2 | xargs ./a.out dlp240.reconstructlog.dlog > $tempfile3

// And we can now craft a dlog file to cheat the python script

head -100000 dlp240.reconstructlog.dlog > dlp240.crafted.dlog
cat $tempfile3 | grep -v "not" >> dlp240.crafted.dlog
tail -10 dlp240.reconstructlog.dlog >> dlp240.crafted.dlog
```

Call the python script that glues things together
=================================================

```
ln -s $wdir/dlp240.crafted.dlog $wdir/dlp240.dlog

# most parameters are not used, since we have precomputed everything !
$CADO_BUILD/scripts/descent.py --target $target --gfpext 1 --prefix dlp240 --datadir $wdir --cadobindir $CADO_BUILD --descent-hint $wdir/dlp240.hint --init-I 10 --init-ncurves 5 --init-lpb 22 --init-lim 1500 --init-mfb 34 --init-tkewness 100000 --I 16 --lpb0 35 --lpb1 35 --mfb0 70 --mfb1 70 --lim0 1073741824 --lim1 1073741824 --ell $ell
```
