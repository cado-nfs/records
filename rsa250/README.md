# RSA250

This repository contains information to reproduce the RSA-250 factoring record.

Several chapters are covered.

 * [Software prerequisites, and reference hardware configuration](#software-prerequisites-and-reference-hardware-configuration)
 * [Searching for a polynomial pair](#searching-for-a-polynomial-pair)
 * [Estimating the number of (unique) relations](#estimating-the-number-of-unique-relations)
 * [Estimating the cost of sieving](#estimating-the-cost-of-sieving)
   * [Cost of 2-sided sieving in the q-range [1e9,4e9]](#cost-of-2-sided-sieving-in-the-q-range-1e94e9)
   * [Cost of 1-sided sieving + batch in the q-range [4e9,12e9]](#cost-of-1-sided-sieving-batch-in-the-q-range-4e912e9)
 * [Estimating linear algebra time (coarsely)](#estimating-linear-algebra-time-coarsely)
 * [Validating the claimed sieving results](#validating-the-claimed-sieving-results)
 * [Reproducing the filtering results](#reproducing-the-filtering-results)
 * [Estimating linear algebra time more precisely, and choosing parameters](#estimating-linear-algebra-time-more-precisely-and-choosing-parameters)
 * [Reproducing the linear algebra results](#reproducing-the-linear-algebra-results)
 * [Reproducing the characters step](#reproducing-the-characters-step)
 * [Reproducing the square root step](#reproducing-the-square-root-step)

Note that this computation has many similarities with the previous 
[RSA-240](../rsa240/README.md) computation.

## Software prerequisites, and reference hardware configuration

This is exactly similar to
[RSA-240](../rsa240/README.md#software-prerequisites-and-reference-hardware-configuration).

We also repeat this important paragraph from the RSA-240 documentation.
Most (if not all) information boxes in this document rely on two shell
variables, `CADO_BUILD` and `DATA`, be set and exported to shell
subprocess (as with `export CADO_BUILD=/blah/... ; export
DATA=/foo/...`). The `CADO_BUILD` variable is assumed to be the path to
a successful cado-nfs build directory. The `DATA` variable, which is
used by some scripts, should point to a directory with plenty of storage,
possibly on some shared filesystem. Storage is also needed to store the
temporary files with collected relations. Overall, a full reproduction of
the computation would need in the whereabouts of 10TB of storage. All
scripts provided in this document expect to be run from the directory where
they are placed, since they are also trying to access companion data
files.

## Searching for a polynomial pair

The polynomial selection size-optimization
step was performed with cado-nfs in client/server mode. We used a mysql database back-end, which supports more concurrent connections to the server, but that is only of minor importance.
```shell
./cado-nfs.py params.rsa250 database=db:mysql://USERNAME:PASSWORD@localhost:3306/rsa250
```
with the file [`params.rsa250`](params.rsa250).

The root-optimization step was performed on the best 100 size-optimized
polynomials (those with the smallest `exp_E` value) with the following command:
```shell
$CADO_BUILD/polyselect/polyselect_ropt -inputpolys candidates -Bf 2.1e9 -Bg 1.8e9 -area 2.4e19 -ropteffort 10 -t 100
```

And the winner is the [`rsa250.poly`](rsa250.poly) file:

```shell
cat > rsa250.poly <<EOF
n: 2140324650240744961264423072839333563008614715144755017797754920881418023447140136643345519095804679610992851872470914587687396261921557363047454770520805119056493106687691590019759405693457452230589325976697471681738069364894699871578494975937497937
poly0: -3256571715934047438664355774734330386901,185112968818638292881913
poly1: -81583513076429048837733781438376984122961112000,-1721614429538740120011760034829385792019395,-3113627253613202265126907420550648326,46262124564021437136744523465879,-52733221034966333966198,-66689953322631501408,86130508464000
skew: 354109.861
# lognorm 69.69, E 60.81, alpha -8.88 (proj -3.27), 4 real roots
# MurphyE(Bf=2.100e+09,Bg=1.800e+09,area=2.400e+19)=2.80e-13
EOF
```

## Estimating the number of (unique) relations

To estimate the number of relations produced by a set of parameters:
- We compute the corresponding factor bases. In our case, the side 0 is
  rational, so there is no need to precompute the factor base. (Note that
  this precomputed factor base is different from what we call the "factor
  base cache", and which also appears later.)
- We create a "hint" file where we tell which strategy to use for which
  special-q size.
- We random-sample in the global q-range, using sieving and not batch:
  this produces the same relations. This is slower but `-batch` is
  incompatible (with the version of cado-nfs we used) with on-the-fly duplicate removal.

Here is what it gives with the parameters that were used in the computation.

The computation of the factor base is done with the following command.
Here, `-t 16` specifies the number of threads (more is practically
useless, since gzip soon becomes the limiting factor).

```shell
$CADO_BUILD/sieve/makefb -poly rsa250.poly -side 1 -lim 2147483647 -maxbits 16 -t 16 -out $DATA/rsa250.fb1.gz
```
The file has size 804409710 bytes, and takes less than 4 minutes 
to compute on the `grvingt` computers.

The hint file is [`rsa250.hint`](rsa250.hint), and has a weird format.
The following basically says "Three algebraic large primes for special-q's
less than 2^32, and two otherwise."

```shell
cat > rsa250.hint <<EOF
30@1 1.0 1.0 A=33 2147483647,36,72,2.2 2147483647,37,111,3.2
31@1 1.0 1.0 A=33 2147483647,36,72,2.2 2147483647,37,111,3.2
32@1 1.0 1.0 A=33 2147483647,36,72,2.2 2147483647,37,111,3.2
33@1 1.0 1.0 A=33 2147483647,36,72,2.2 2147483647,37,74,2.2
34@1 1.0 1.0 A=33 2147483647,36,72,2.2 2147483647,37,74,2.2
EOF
```

We can now sieve for random-sampled special-q's, and remove duplicate
relations on-the-fly. In the output of the command line below, only the
number of unique relations per special-q matters. The timing does not
matter.

```shell
$CADO_BUILD/sieve/las -poly rsa250.poly -fb1 $DATA/rsa250.fb1.gz -lim0 2147483647 -lim1 2147483647 -lpb0 36 -lpb1 37 -q0 1e9 -q1 12e9 -dup -dup-qmin 0,1000000000 -sqside 1 -A 33 -mfb0 72 -mfb1 111 -lambda0 2.2 -lambda1 3.2 -random-sample 1024 -t auto -bkmult 1,1l:1.25975,1s:1.5,2s:1.1 -v -bkthresh1 80000000 -adjust-strategy 2 -fbc /tmp/rsa250.fbc -hint-table rsa250.hint
```

In approximately three hours on our target machine `grvingt`, this gives
the estimate that 12.7 unique relations per special-q can be expected
based on these parameters. (Note that this test can also be done in
parallel over several nodes, using the `-seed [[seed value]]` argument in
order to vary the random picks.)

In order to deduce an estimate of the total number of (de-duplicated)
relations, it remains to multiply the average number of relations per
special-q as obtained during the sample sieving by the number of
special-q's in the global q-range. The latter can be precisely estimated
using the logarithmic integral function as an approximation of the number
of degree-1 prime ideals below a bound. In
[Sagemath](https://www.sagemath.org/) code, this gives:

```python
# [sage]
ave_rel_per_sq = 12.7   ## pick value output by las
number_of_sq = log_integral(12e9) - log_integral(1e9)
tot_rels = ave_rel_per_sq * number_of_sq
print (tot_rels)
# 6.23205306433878e9
```
This estimate (6.2G relations) can be made more precise by increasing the
number of special-q's that are sampled for sieving. It is also possible to
have different nodes sample different sub-ranges of the global range to
get the result faster. We consider that sampling 1024 special-q's is
enough to get a reliable estimate.

## Estimating the cost of sieving

The sieving step requires a lot of computing power and therefore many
different machines were used during the record computation. It is
interesting to normalize the cost for a single typical machine, for which
we do measurements "under lab conditions":
- no known hardware or software issues;
- appropriate cooling so that the processors can use their top frequency;
- no other job running at the same time, apart from standard OS
  functioning;
- no slow disk access due to competing I/Os.

On the `grvingt` cluster that we use for the measure, these perfect
conditions were reached during production as well, most of the time.

In production there is no need to activate the on-the-fly duplicate
removal which is supposed to be cheap but maybe not negligible. There is
also no need to pass the hint file, since we are going to run the siever
on different parts of the q-range, and on each of them the parameters are
constant. Finally, during a benchmark, it is important to emulate the
fact that the cached factor base (the `/tmp/rsa250.fbc` file) is
precomputed and hot (i.e., cached in memory by the OS and/or the
hard-drive), because this is the situation in production; for this, it
suffices to start a first run and interrupt it as soon as the cache is
written (or pass `-nq 0`).

#### Cost of 2-sided sieving in the q-range [1e9,4e9]

In order to measure the cost of sieving in the special-q subrange where
sieving is used on both sides, the typical command line is as follows:

```shell
time $CADO_BUILD/sieve/las -poly rsa250.poly -fb1 $DATA/rsa250.fb1.gz -lim0 2147483647 -lim1 2147483647 -lpb0 36 -lpb1 37 -q0 1e9 -q1 4e9 -sqside 1 -A 33 -mfb0 72 -mfb1 111 -lambda0 2.2 -lambda1 3.2 -random-sample 1024 -t auto -bkmult 1,1l:1.25975,1s:1.5,2s:1.1 -v -bkthresh1 80000000 -adjust-strategy 2 -fbc /tmp/rsa250.fbc
```

(note: the first time this command line is run, it takes some time to
create the "cache" file `/tmp/rsa250.fbc`. If you want to avoid this, you
may run the command with `-random-sample 1024` replaced by
`-random-sample 0` first, which will _only_ create the cache file. Then
run the command above.)

While `las` tries to print some running times, some start-up or finish
tasks might be skipped; furthermore the CPU-time gets easily confused by
the hyperthreading. Therefore, it is better to rely on `time`, since this
gives the real wall-clock time exactly as it was taken by the
computation.

On our sample machine, the result of the above line is:
```
real    128m8.106s
user    7845m52.875s
sys     70m15.469s
```
Then the `128m8.106=7688.1s` value must be appropriately scaled in order
to convert it into physical core-seconds. For instance, in our case,
since there are 32 physical cores and we sieved 1024 special-q's, this
gives `(128*60+8.1)*32/1024=240.25` core.seconds per special-q.

Finally, it remains to multiply by the number of special-q's in this
subrange. We get (in Sagemath):

```python
# [sage]
cost_in_core_sec=(log_integral(4e9)-log_integral(1e9))*(128*60+8.1)*32/1024
cost_in_core_hours=cost_in_core_sec/3600
cost_in_core_years=cost_in_core_hours/24/365
print (cost_in_core_hours, cost_in_core_years)
# (9.28413860267641e6, 1059.83317382151)
```

With this experiment, we get therefore about 1060 core.years for this sub-range.


#### Cost of 1-sided sieving + batch in the q-range [4e9,12e9]

For special-q's larger than 4e9, since we are using batch smoothness
detection on side 0, we have to precompute the `rsa250.batch0` file which
contains the product of all primes to be extracted. (Note that the
`-batch1` option is mandatory, even if for our parameters, no file is
produced on side 1.)

```shell
$CADO_BUILD/sieve/ecm/precompbatch -poly rsa250.poly -lim0 0 -lim1 2147483647 -batch0 $DATA/rsa250.batch0 -batch1 $DATA/rsa250.batch1 -batchlpb0 31 -batchlpb1 30
```

Then, we can use the [`rsa250-sieve-batch.sh`](rsa250-sieve-batch.sh)
shell script given in this repository. This launches:
- one instance of `las`, which does the sieving on side 1 and prints the
  survivors to files;
- 6 instances of the `finishbatch` program. Those instances process the
  files as they are produced, do the batch smoothness detection, and
  produce relations.

The script takes two command line arguments `-q0 xxx` and `-q1 xxx`,
which describe the range of special-q's to process. Temporary files are put
in the `/tmp` directory by default.

In order to run [`rsa250-sieve-batch.sh`](rsa250-sieve-batch.sh) on your
own machine, there are some variables to adjust at the beginning of the
script. Two examples are already given, so this should be easy to
imitate. The number of instances of finishbatch can also be adjusted
depending on the number of cores available on the machine.

When the paths are properly set (either by having `CADO_BUILD` and
`DATA` set correctly, or by tweaking the script), a typical invocation
is as follows:
```shell
./rsa250-sieve-batch.sh -q0 4000000000 -q1 4000100000
```
The script prints on stdout the start and end date, and in the output of
`las`, which can be found in `$DATA/log/las.${q0}-${q1}.out`, the
number of special-q's that have been processed can be found. From this
information one can again deduce the cost in core.seconds to process one
special-q and then the overall cost of sieving the q-range [4e9,12e9].

The design of this script imposes to have a rather long range of
special-q's to handle for each run of `rsa250-sieve-batch.sh`. Indeed,
during the last minutes, the `finishbatch` jobs need to take care of the
last survivor files while `las` is no longer running, so that the node is
not fully occupied. If the `rsa250-sieve-batch.sh` job takes a few hours,
this fade-out phase takes negligible time. Both for the benchmark and in
production it is then necessary to have jobs taking at least a few hours.

On our sample machine, here is an example of a benchmark:
```shell
./rsa250-sieve-batch.sh -q0 4000000000 -q1 4000100000 > /tmp/rsa250-sieve-batch.out

# [ wait ... ]

start=$(date -d "`grep "^Starting" /tmp/rsa250-sieve-batch.out | head -1 | awk -F " at " '//{print $2}'`" +%s)
end=$(date -d "`grep "^End" /tmp/rsa250-sieve-batch.out | tail -1 | awk -F " at " '//{print $2}'`" +%s)
nb_q=`grep "# Discarded 0 special-q's out of" /tmp/log/las.4000000000-4000100000.out | awk '{print $(NF-1)}'`
echo -n "Cost in core.sec per special-q: "; echo "($end-$start)/$nb_q*32" | bc -l
# Cost in core.sec per special-q: 116.51841746248294679392
```

```python
# [sage]
cost_in_core_sec=(log_integral(12e9)-log_integral(4e9))*116.5
cost_in_core_hours=cost_in_core_sec/3600
cost_in_core_years=cost_in_core_hours/24/365
print (cost_in_core_hours, cost_in_core_years)
# (1.13780852428233e7, 1298.86817840449)
```

With this experiment, we get 116.5 core.sec per special-q, and therefore
we obtain about 1300 core.years for this sub-range.

## Estimating linear algebra time (coarsely)

The general description of the [RSA-240
case](../rsa240/README.md#estimating-the-linear-algebra-time-coarsely)
applies identically to RSA-250.

We reproduce here the adaptations of the command lines to the RSA-250
case. To estimate the time per iteration of a matrix with (say) 400M
rows/columns and density 250, it is possible to do the following.

```
nrows=400000000 density=250 nthreads=32 ./rsa250-linalg-0a-estimate_linalg_time_coarse_method_b.sh
```

This reports about 1.3 seconds per iteration, yielding an
anticipated cost of
`(1+n/m+64/n)*(N/64)*1.3*16*32/3600/24/365=206` core-years
for Krylov+Mksol.

## Validating the claimed sieving results

The benchmark command lines above can be used almost as is to reproduce
the full computation. It is just necessary to remove the `-random-sample`
option and to adjust the `-q0` and `-q1` to create many small work units
that in the end cover exactly the global q-range.

Since we do not expect anyone to spend as much computing resources
to perform again exactly the same computation, we provide in the
[`rsa250-rel_count`](rsa250-rel_count) file the count of how many (non-unique)
relations were produced for each 100M special-q sub-range.

We can then have a visual plot of this data, as shown in
[`rsa250-plot_rel_count.pdf`](rsa250-plot_rel_count.pdf) where we see the
drop in the number of relations produced per special-q when changing the
strategy.  The plot is very regular on the two sub-ranges.

In order to validate our computation, it is possible to re-compute only
one of the sub-ranges and check that the number of relations is the one
we report. This still requires significant resources. If only a single
node is available for the validation, it is better to rely on sampling as
for the estimates in previous sections, and extrapolate.

## Reproducing the filtering results

The filtering follows roughly the same general workflow as in the
[RSA-240 case](../rsa240/filtering.md), with no significant changes.
Several filtering experiments were done during the sieving phase.
The final one can be reproduced as follows, with revision `f59dcf48f`:
```shell
$CADO_BUILD/filter/purge -out $DATA/purged3.gz -nrels 6132671469 -keep 160 -col-min-index 0 -col-max-index 8460769956 -t 56 -required_excess 0.0 files
```
where `files` is the list of files with unique relations (output of
`dup2`).  This took about 9.3 hours on the machine `wurst`, with 138GB of
peak memory.

The merge step can be reproduced as follows (revision `eaeb2053d`):
```shell
$CADO_BUILD/filter/merge -out $DATA/history5 -t 112 -target_density 250 -mat $DATA/purged3.gz -skip 32
```
and took about 3 hours on the machine wurst, with a peak memory of 1500GB.

Finally the replay step can be reproduced as follows. Note that we
created the two data files `$DATA/rsa250.matrix.250.bin` and
`$DATA/rsa250.index.gz` in two separate steps, in order to save memory
(the first command line below, which generates the matrix file, took 15
hours and used 1.4TB of RAM).
```shell
$CADO_BUILD/filter/replay -purged $DATA/purged3.gz -his $DATA/history5 -out $DATA/rsa250.matrix.250.bin
$CADO_BUILD/filter/replay -purged $DATA/purged3.gz -his $DATA/history5 -index $DATA/rsa250.index.gz
```

The matrix that we eventually selected has 404711409 rows, and an average
density of 251.9 coefficients per row. The size of the file
`$DATA/rsa250.matrix.250.bin` is 382GB.

## Estimating linear algebra time more precisely, and choosing parameters

Again, the general description of the [RSA-240
case](../rsa240/README.md#estimating-the-linear-algebra-time-more-precisely-and-choosing-parameters)
applies identically to RSA-250.

In order to benchmark the time per iteration for the selected matrix, we can
use the following script.
```shell
export matrix=$DATA/rsa250.matrix.250.bin
export DATA
export CADO_BUILD
export MPI
./rsa250-linalg-0b-test-few-iterations.sh
```

This reports an average of 1.3 to 1.4 seconds per iteration.

## Reproducing the linear algebra results

We decided to use the block Wiedemann parameters `m=1024` and `n=512`,
giving rise to `n/64=8` sequences to be computed indepedently. We used
16-node jobs.

The first part of the computation can be done with these scripts:
```shell
export matrix=$DATA/rsa250.matrix.250.bin
export DATA
export CADO_BUILD
export MPI
./rsa250-linalg-1-prep.sh
./rsa250-linalg-2-secure.sh
./rsa250-linalg-3-krylov.sh sequence=0 start=0
# [...] 14 more
./rsa250-linalg-3-krylov.sh sequence=15 start=0
```

Once this is done, data must be collated before being processed by the
later steps. After step `5-acollect` below, a file named
`A0-512.0-1185792` with size 77712064512 bytes will be in `$DATA`. Step
`6-lingen` below runs on 64 nodes, and completes in approximately 3 days.

```shell
export matrix=$DATA/rsa250.matrix.250.bin
export DATA
export CADO_BUILD
export MPI
./rsa250-linalg-5-acollect.sh
./rsa250-linalg-6-lingen.sh
# there is no step 7, for consistency with the discrete log case.
./rsa250-linalg-8-mksol.sh start=0
./rsa250-linalg-8-mksol.sh start=32768
./rsa250-linalg-8-mksol.sh start=65536
# ... 21 other commands of the same kind (25 in total) ...
./rsa250-linalg-8-mksol.sh start=786432
./rsa250-linalg-9-finish.sh
```

After having successfully followed the steps above, a file named
`W.sols0-64` will be in `$DATA` (with a symlink to it called `W`). This
file represents a kernel vector.

## Reproducing the characters step

Let `W` be the kernel vector computed by the linear algebra step.
The characters step transforms this kernel vector into dependencies.
We used the following command on the machine `wurst`:
```shell
$CADO_BUILD/linalg/characters -poly rsa250.poly -purged $DATA/purged3.gz -index $DATA/rsa250.index.gz -heavyblock $DATA/rsa250.matrix.250.dense.bin -out $DATA/rsa250.kernel -ker $DATA/W -lpb0 36 -lpb1 37 -nchar 50 -t 56
```
This gave 23 dependencies after about 2 hours.

## Reproducing the square root step

The following command line can be used to produce the actual dependencies
(`rsa250.dep.000.gz` to `rsa250.dep.022.gz`) from the `rsa250.kernel` file:
```shell
$CADO_BUILD/sqrt/sqrt -poly rsa250.poly -prefix $DATA/rsa250.dep.gz -purged $DATA/purged3.gz -index $DATA/rsa250.index.gz -ker $DATA/rsa250.kernel -t 56 -ab
```
Then the following command line can be used to compute the square root on the
rational side of dependency `nnn`:
```shell
$CADO_BUILD/sqrt/sqrt -poly rsa250.poly -prefix $DATA/rsa250.dep -dep nnn -side0
```
and similarly on the algebraic side:
```shell
$CADO_BUILD/sqrt/sqrt -poly rsa250.poly -prefix $DATA/rsa250.dep -dep nnn -side1
```
Then we can simply compute `gcd(x-y,N)` where `x` and `y` are the square
roots on the rational and algebraic sides respectively, and `N` is RSA-250.
