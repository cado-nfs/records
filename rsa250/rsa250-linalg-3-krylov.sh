#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD matrix MPI DATA start sequence ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in krylov ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 16
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    --mca mtl ofi
    --mca mtl_ofi_prov psm2
    --mca btl '^openib'
)
cargs=(
    m=1024 n=512 prime=2
    mpi=4x4 thr=4x16
    sequential_cache_build=16
    sequential_cache_read=2
    wdir=$DATA
    matrix="$matrix"
    cpubinding="Package=>2x1 L2Cache*16=>2x16"
)
kargs=(
    start=$start
    ys=$((sequence*64))..$((sequence*64+64))
    interval=2048
    keep_rolling_checkpoints=4
    checkpoint_precious=32768
)

cbase=$(basename $matrix .bin).16x64
cdir=$DATA/$cbase
cache_files=()
if [ -d "$cdir" ] ; then cache_files=(`find $cdir/ -name $cbase.*.bin`) ; fi
if [ "${#cache_files[@]}" != 1024 ] ; then
    # regenerate caches. this tends to have a negative effect on the
    # runtime, so we'll do _just_ that at first, and restart the full
    # process afterwards.
    for f in "${matrix%bin}"{rw,cw}".bin" ; do
        if ! (cd "$DATA" ; [ -f "$f" ]) ; then echo "$f must exist" >&2 ; exit 1 ; fi
    done
    "${mpi[@]}" $CADO_BUILD/linalg/bwc/dispatch "${cargs[@]}" ys=0..64
fi
"${mpi[@]}" $CADO_BUILD/linalg/bwc/krylov "${cargs[@]}" "${kargs[@]}"
