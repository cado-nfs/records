#!/bin/bash

# This script is buggy, as of commit 8a72ccdde in cado-nfs.
# There are two problems.

# * The locally generated random matrices do not fit the claimed
#   distribution.
#
#   For a matrix with 300M rows/cols and expected density 200, generated
#   over 8*64 threads, it seems that the "density" parameters must be
#   multiplied by the ratio 5/8=0.625, i.e. set to 125, in order to obtain
#   a matrix with the desired characteristics.
#
# * Furthermore, the timings of the communication step seem to be
#   somewhat off compared to what we get in production runs (production
#   runs achieve better performance), and we have no explanation for it.
#

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD MPI ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in krylov ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

: ${nrows=300000000}
: ${density=200}

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 8
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    --mca mtl ofi
    --mca mtl_ofi_prov psm2
    --mca btl '^openib'
)
kargs=(
    m=128 n=64 prime=2
    wdir=/tmp
    mpi=4x2 thr=2x32
    sequential_cache_build=16
    ys=0..64
    random_matrix=$nrows,density=$density
    cpubinding="Package=>2x1 L2Cache*16=>1x32 PU*2=>1x1"
    start=0 interval=128 end=128
)
"${mpi[@]}" $CADO_BUILD/linalg/bwc/krylov "${kargs[@]}"
