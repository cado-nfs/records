#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD MPI DATA ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in lingen_p_13 ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 36
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    # https://github.com/open-mpi/ompi/issues/7058 -- still
    # no reliable way to use ofi
    --mca mtl ^psm2,ofi,cm
    --mca btl '^openib'
)
mkdir $DATA/cp || :
args=(
    m=48 n=16
    prime=62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651
    mpi=6x6
    wdir=$DATA
    --afile A0-16.0-3016704
    --ffile F
    --split-output-file 1
    tuning_timing_cache_filename="`dirname $0`"/dlp240-linalg-6-lingen-tim240.txt
    tuning_schedule_filename="`dirname $0`"/dlp240-linalg-6-lingen-ts240.txt
    max_ram=128
    # thr=8x4 (do not specify thr)
    tree_stats_max_nesting=3
    basecase_keep_until=1.1
    checkpoint_directory=cp
    checkpoint_threshold=131072
    logline_threshold=1024,16384
    logline_timings=1
)
"${mpi[@]}" $CADO_BUILD/linalg/bwc/lingen_p_13 "${args[@]}"
