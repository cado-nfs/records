#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <gmp.h>

// usage: ./$0 <dlog_file> side,q,r side,q,r ...

void advance_nextline(FILE* file) {
    int c;
    do {
        c = fgetc(file);
    } while ((char)c != '\n');
}

typedef struct {
    int side;
    unsigned long q;
    unsigned long r;
} ideal_t;

int id_eq(ideal_t id1, ideal_t id2) {
    return (id1.side == id2.side) && (id1.q == id2.q) && (id1.r == id2.r);
}

void get_curr_ideal(FILE *file, ideal_t * qq, mpz_t dlog) {
    char s[2048];
    fgets(s, 2048, file);
    char *str = &s[0];
    char *token = strsep(&str, " "); // skip index
    token = strsep(&str, " ");
    unsigned long q = strtoul(token, NULL, 16);
    token = strsep(&str, " ");
    int side = atol(token);
    token = strsep(&str, " ");
    unsigned long r = strtoul(token, NULL, 16);
    token = strsep(&str, " ");
    mpz_set_str(dlog, token, 10);
    qq->side = side;
    qq->q = q;
    qq->r = r;
}

int main(int argc, char *argv[])
{
    FILE* file = fopen(argv[1], "r");
    assert(file != NULL);

    // Begining and end of file, up to some margin to avoid bad ideals,
    // SMs, and co.
    unsigned long beg = 10000;
    fseek(file, -10000, SEEK_END);
    unsigned long end = ftell(file);

    mpz_t log;
    mpz_init(log);

    for (int i = 2; i < argc; ++i) {
        int side;
        unsigned long q, r;
        char *str = argv[i];
        char *token = strsep(&str, ",");
        side = atoi(token);
        token = strsep(&str, ",");
        q = atol(token);
        token = strsep(&str, ",");
        r = atol(token);
        ideal_t qq_wanted = {side, q, r};

        printf("idx %lx %d %lx ", q, side, r);

        ideal_t qq_low, qq_high;
        unsigned long low = beg;
        unsigned long high = end;

        fseek(file, low, SEEK_SET);
        advance_nextline(file);
        get_curr_ideal(file, &qq_low, log);
        
        fseek(file, high, SEEK_SET);
        advance_nextline(file);
        get_curr_ideal(file, &qq_high, log);

        while (!id_eq(qq_low, qq_high)) {
            // small range: process the whole range.
            if ((high - low) < 10000) {
                fseek(file, low, SEEK_SET);
                advance_nextline(file);
                do {
                    get_curr_ideal(file, &qq_low, log);
                } while (!(id_eq(qq_low,qq_wanted) || id_eq(qq_low, qq_high)));
                if (id_eq(qq_low, qq_wanted)) {
                    gmp_printf("%Zd\n", log);
                } else {
                    printf("not found !\n");
                }
                break;
            } else {
                unsigned long mid = (low + high) / 2;
                ideal_t qq_mid;
                fseek(file, mid, SEEK_SET);
                advance_nextline(file);
                get_curr_ideal(file, &qq_mid, log);
                if (id_eq(qq_mid,qq_wanted)) {
                    gmp_printf("%Zd\n", log);
                    break;
                }
                if (qq_mid.q < qq_wanted.q) {
                    qq_low = qq_mid;
                    low = mid;
                } else if (qq_mid.q == qq_wanted.q) {
                    // sorting is not clear, in that case...
                    low += 100;
                    fseek(file, low, SEEK_SET);
                    advance_nextline(file);
                    get_curr_ideal(file, &qq_low, log);
                } else {
                    qq_high = qq_mid;
                    high = mid;
                }
            }
        }
    }

    mpz_clear(log);
    fclose(file);
}


    


