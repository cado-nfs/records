#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD DATA MPI ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in random_matrix mf_scan2 dispatch krylov ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

: ${nrows=37000000}
: ${density=250}
export nthreads=32 matrixname="$DATA/test.bin"
export matrix="$matrixname"
if ! [ -f $matrixname ] ; then
    export nrows
    export density
    export dlp=true
    "`dirname $0`"/generate_random_matrix.sh
fi

set -ex

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 4
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    --mca mtl ofi
    --mca mtl_ofi_prov psm2
    --mca btl '^openib'
)
cargs=(
    m=2 n=1
    prime=62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651
    mpi=2x2 thr=16x4
    sequential_cache_build=16
    sequential_cache_read=2
    ys=0..1
    wdir=$DATA
    static_random_matrix="$matrix"
    cpubinding="Package=>1x2 L2Cache*16=>16x2 PU*2=>1x1"
)

cbase=$(basename $matrix .bin).32x8
cdir=$DATA/$cbase
cache_files=()
if [ -d "$cdir" ] ; then cache_files=(`find $cdir/ -name $cbase.*.bin`) ; fi
if [ "${#cache_files[@]}" != 256 ] ; then
    # regenerate caches. this tends to have a negative effect on the
    # runtime, so we'll do _just_ that at first, and restart the full
    # process afterwards.
    "${mpi[@]}" $CADO_BUILD/linalg/bwc/dispatch "${cargs[@]}"
fi
"${mpi[@]}" $CADO_BUILD/linalg/bwc/krylov "${cargs[@]}" start=0 interval=128 end=128
