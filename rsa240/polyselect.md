# Additional info on polynomial selection for RSA-240

This file contains reproducibility information for the polynomial selection
step of the factorization of RSA-240.

We used revision `52ac92746` of cado-nfs.

The first stage of polynomial selection (size optimization) was equivalent to
the following pseudo-code

```
for admin from 0 to 2000000000000 by 2500000:
   admax = admin + 2500000
   polyselect -P 20000000 -N 124620366781718784065835044608106590434820374651678805754818788883289666801188210855036039570272508747509864768438458621054865537970253930571891217684318286362846948405301614416430468066875699415246993185704183030512549594371372159029236099 -degree 6 -t 1 -admin $admin -admax $admax -incr 110880 -nq 1296 -sopteffort 20
```

We found 39890071 size-optimized polynomials, we kept the 104 most promising
ones (i.e., the ones with the smallest `exp_E` value). Among them, the best `exp_E` was 57.78, the worst `exp_E` was 59.49.

We used the following command line for root optimization (still with cado-nfs
revision `52ac92746`), where `candidates` is the file containing all candidates
kept after size optimization:

```
$CADO_BUILD/polyselect/polyselect_ropt -inputpolys candidates -Bf 2.1e9 -Bg 1.8e9 -area 2.4e19 -ropteffort 10 -t 104
```

The polynomial used for the factorization was the best polynomial output
at the end of the `polyselect_ropt` run (the one with the largest MurphyE
value, namely 7.41e-13).

We also did a sieving test for all 104 root-optimized polynomials, using the
following command (where `$1` is the corresponding root-optimized file):

```
$CADO_BUILD/sieve/makefb -poly $1 -lim 2100000000 -maxbits 17 -out /tmp/`basename $1`.roots -t 64
$CADO_BUILD/sieve/las -poly $1 -fb1 /tmp/`basename $1`.roots -lim0 1800000000 -lim1 2100000000 -lpb0 36 -lpb1 37 -q0 1e8 -q1 3e9 -dup -dup-qmin 0,100000000 -sqside 1 -A 33 -mfb0 72 -mfb1 111 -lambda0 2.0 -lambda1 3.0 -random-sample 128 -t auto -bkmult 1,1l:1.15,1s:1.4,2s:1.1 -v -bkthresh1 80000000
```

The polynomial with the largest average number of relations per special-q
was the one with the largest MurphyE value, with an average of 48.6 relations
per special-q.
