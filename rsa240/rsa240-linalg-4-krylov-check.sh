#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD DATA ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in bwccheck ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

cargs=(m=512 n=256 prime=2 wdir=$DATA)

files=(`cd $DATA > /dev/null ; find -maxdepth 1 -type f -a \
            \( -name "[AV]*-*.*" -o -name 'C[rt]*' \
            -o -name 'C[dv]0-64.*' \)`)

$CADO_BUILD/linalg/bwc/bwccheck "${cargs[@]}" -- "${files[@]}"
