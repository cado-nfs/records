# DLP-240

This repository contains information to reproduce the DLP-240 discrete
logarithm record.

There are several subsections.

* [Software prerequisites, and reference hardware configuration](#software-prerequisites-and-reference-hardware-configuration)
 * [Searching for a polynomial pair](#searching-for-a-polynomial-pair)
 * [Estimating the number of (unique) relations](#estimating-the-number-of-unique-relations)
 * [Estimating the cost of sieving](#estimating-the-cost-of-sieving)
 * [Estimating the linear algebra time (coarsely)](#estimating-the-linear-algebra-time-coarsely)
 * [Validating the claimed sieving results](#validating-the-claimed-sieving-results)
 * [Reproducing the filtering results](#reproducing-the-filtering-results)
   * [Duplicate removal](#duplicate-removal)
   * [The "purge" step, a.k.a. singleton and "clique" removal](#the-purge-step-aka-singleton-and-clique-removal)
   * [The "merge" step](#the-merge-step)
   * [The "replay" step](#the-replay-step)
   * [Computing the right-hand side](#computing-the-right-hand-side)
 * [Estimating the linear algebra time more precisely, and choosing parameters](#estimating-the-linear-algebra-time-more-precisely-and-choosing-parameters)
 * [Reproducing the linear algebra results](#reproducing-the-linear-algebra-results)
 * [Back-substituting the linear algebra result in collected relations](#back-substituting-the-linear-algebra-result-in-collected-relations)
 * [Reproducing the individual logarithm result](#reproducing-the-individual-logarithm-result)

## Software prerequisites, and reference hardware configuration

This is similar to
[RSA-240](../rsa240/README.md#software-prerequisites-and-reference-hardware-configuration).
As in the RSA-240 case, this documentation relies on [commit 8a72ccdde of
cado-nfs](https://gitlab.inria.fr/cado-nfs/cado-nfs/commit/8a72ccdde) as
baseline. In some cases we provide exact commit numbers for specific
commands as well.

We also reiterate this important paragraph from the RSA-240 documentation.
Most (if not all) information boxes in this document rely on two shell
variables, `CADO_BUILD` and `DATA`, be set and `export`-ed to shell
subprocess (as with `export CADO_BUILD=/blah/... ; export
DATA=/foo/...`). The `CADO_BUILD` variable is assumed to be the path to
a successful cado-nfs build directory. The `DATA` variable, which is
used by some scripts, should point to a directory with plenty of storage,
possibly on some shared filesystem. Storage is also needed to store the
temporary files with collected relations. Overall, a full reproduction of
the computation would need in the vicinity of 20TB of storage. All
scripts provided in this document expect to be run from the directory where
they are placed, since they are also trying to access companion data
files.

## Searching for a polynomial pair

We searched for a polynomial pair _à la_ Joux-Lercier, using the
program `dlpolyselect` of `cado-nfs`, with parameters `-bound 150`,
`-degree 4` and `-modm 1000003`. We also tried to search for a skewed
pair of polynomials with the `-skewed` option, but this did not seem to
give a better polynomial for a fixed amount of time compared to plain
flat polynomials.

A typical command line for an individual work unit was:
```shell
$CADO_BUILD/polyselect/dlpolyselect -N 124620366781718784065835044608106590434820374651678805754818788883289666801188210855036039570272508747509864768438458621054865537970253930571891217684318286362846948405301614416430468066875699415246993185704183030512549594371372159029285303 -df 4 -dg 3 -area 2.0890720927744e+20 -Bf 34359738368.0 -Bg 34359738368.0 -bound 150 -modm 1000003 -modr 42 -t 8
```
where `-modr 42` gives the index of the task, and all tasks between 0 and
1000002 were run.

A ranking between all the computed pairs was based on MurphyE as computed
by `dlpolyselect` with the parameters
```
  Bf=3.436e+10
  Bg=3.436e+10
  area=2.089e+20
```
which are the default parameters automatically computed by the server
script `cado-nfs.py` that we used for distributing the computation. These
might be imprecise, but the ranking is fairly stable when parameters
are changed.

This polynomial selection took place on a variety of computer resources,
but more than half of it was done on the `grvingt` cluster that we take
as a reference. The program was improved during the computation,  so it
makes little sense to report the total number of CPU-years used in reality.
The calendar time was 18 days. 

When a node of `grvingt` is fully occupied with 8 jobs of 8 threads, then
one task as above is processed in 1200 wall-clock seconds on average.
This must be multiplied by the 4 physical cores it uses to get
the number of core-seconds per modr value. We have 10^6 of them to
process. This adds up to 152 core-years for the whole polynomial selection.

Some sample-sieving was done on the top 100 polynomials according to
MurphyE. Although there is a clear correlation between the efficiency of
a polynomial pair and its MurphyE value, the ranking is definitely not perfect.
In particular, the best ranked polynomial pair according to MurphyE finds
10% fewer relations than the actual best-performing ones.

Additional sample sieving was performed on the few best candidates. With
a test on 128,000 special-q's, 3 polynomials could not be separated.

We ended up using the following [`dlp240.poly`](dlp240.poly):

```shell
cat > dlp240.poly <<EOF
n: 124620366781718784065835044608106590434820374651678805754818788883289666801188210855036039570272508747509864768438458621054865537970253930571891217684318286362846948405301614416430468066875699415246993185704183030512549594371372159029285303
poly0: -236610408827000256250190838220824122997878994595785432202599,-18763697560013016564403953928327121035580409459944854652737,24908820300715766136475115982439735516581888603817255539890,286512172700675411986966846394359924874576536408786368056
poly1: 120,62,1,126,39
skew: 2.62
# f lognorm 4.82, alpha -1.34, score 3.48, 2 rroots
# g lognorm 134.47, alpha -4.56, score 129.90, 3 rroots
# f+g score 133.38
# MurphyE(Bf=3.436e+10,Bg=3.436e+10,area=2.089e+20)=8.39e-11
EOF
```

## Estimating the number of (unique) relations

To estimate the number of relations produced by a set of parameters:
- We compute the corresponding factor bases.
- We randomly sample in the global q-range, using sieving instead of batch:
  this produces the same relations. This is slower but `-batch` is
  incompatible (with the version of cado-nfs we used) with on-the-fly duplicate removal.

Here is the result with the final parameters used in the computation. Here,
`-t 16` specifies the number of threads (more is essentially useless, since
gzip soon becomes the limiting factor).

```shell
$CADO_BUILD/sieve/makefb -poly dlp240.poly -side 0 -lim 536870912 -maxbits 16 -t 16 -out $DATA/dlp240.fb0.gz
$CADO_BUILD/sieve/makefb -poly dlp240.poly -side 1 -lim 268435456 -maxbits 16 -t 16 -out $DATA/dlp240.fb1.gz
```

These files have size 209219374 and 103814592 bytes, respectively. They
take less than a minute to compute.

We can now sieve for randomly sampled special-q's, removing duplicate
relations on the fly. In the output of the command line below, only the
number of unique relations per special-q matters. The timing does not
matter.

```shell
$CADO_BUILD/sieve/las -poly dlp240.poly -fb0 $DATA/dlp240.fb0.gz -fb1 $DATA/dlp240.fb1.gz -lim0 536870912 -lim1 268435456 -lpb0 35 -lpb1 35 -q0 150e9 -q1 300e9 -dup -dup-qmin 150000000000,0 -sqside 0 -A 31 -mfb0 70 -mfb1 70 -lambda0 2.2 -lambda1 2.2 -random-sample 1024 -allow-compsq -qfac-min 8192 -qfac-max 100000000 -allow-largesq -bkmult 1.10 -t auto -v -fbc /tmp/dlp240.fbc 
```

In less than half an hour on our target machine `grvingt`, this gives
the estimate that we can expect 0.61 unique relations per special-q 
based on these parameters. (Note that this test can also be done in
parallel over several nodes, using the `-seed [[seed value]]` argument in
order to vary the random choices.)

In order to deduce an estimate of the total number of (de-duplicated)
relations, we need to multiply the average number of relations per
special-q obtained during the sample sieving by the number of
special-q's in the global q-range. This number of composite special-q's can
be computed exactly or estimated using the logarithmic integral function.
For the target interval, there are 3.67e9 special-q's.

```python
# [sage]
ave_rel_per_sq = 0.61   ## pick value output by las
number_of_sq = 3.67e9
tot_rels = ave_rel_per_sq * number_of_sq
print (tot_rels)
```
This estimate (2.2G relations) can be made more precise by increasing the
number of special-q's that are sampled for sieving. It is also possible to
have different nodes sample different sub-ranges of the global range to
get the result more quickly. We consider sampling 1024 special-q's to be
enough to get a reliable estimate.

## Estimating the cost of sieving

In production there is no need to activate the on-the-fly duplicate
removal, which is supposed to be cheap but maybe not negligible, and it is
important to emulate the fact that the cached factor base (the
`/tmp/dlp240.fbc` file) is precomputed and hot (i.e., cached in memory by
the OS and/or the hard-drive), because this is the situation in
production; for this, it suffices to start a first run and interrupt it
as soon as the cache is written. Of course, we use the batch smoothness
detection on side 1 and we have to precompute the product of all primes
to be extracted. This means that on the other hand, the file
`$DATA/dlp240.fb1.gz` is _not_ needed in production (we only used it for
the estimation of the number of unique relations).

```shell
$CADO_BUILD/sieve/ecm/precompbatch -poly dlp240.poly -lim1 0 -lim0 536870912 -batch0 /dev/null -batch1 $DATA/dlp240.batch1 -batchlpb0 29 -batchlpb1 28
```

Then a typical benchmark is as follows:

```shell
time $CADO_BUILD/sieve/las -v -poly dlp240.poly -t auto -fb0 $DATA/dlp240.fb0.gz -allow-compsq -qfac-min 8192 -qfac-max 100000000 -allow-largesq -A 31 -lim1 0 -lim0 536870912 -lpb0 35 -lpb1 35 -mfb1 250 -mfb0 70 -batchlpb0 29 -batchlpb1 28 -batchmfb0 70 -batchmfb1 70 -lambda1 5.2 -lambda0 2.2 -batch -batch1 $DATA/dlp240.batch1 -sqside 0 -bkmult 1.10 -q0 150e9 -q1 300e9 -fbc /tmp/dlp240.fbc -random-sample 2048
```

(note: the first time this command line is run, it takes some time to
create the "cache" file `/tmp/dlp240.fbc`. If you want to avoid this, you
may run the command with `-random-sample 1024` replaced by
`-random-sample 0` first, which will _only_ create the cache file. Then
run the command above.)

On our sample machine, the result of the above line is:
```
real    22m19.032s
user    1315m10.459s
sys     5m56.262s
```
Then the `22m19.032s` value must be appropriately scaled in order to
convert it to physical core-seconds. For instance, in our
case, since there are 32 cores and we sieved 2048 special-q's, this gives
`(22*60+19.0)*32/2048=20.9` core-seconds per special-q.

Finally, we need to multiply by the number of special-q's in this
subrange. We get (in Sagemath):

```python
# [sage]
cost_in_core_sec=3.67e9*20.9
cost_in_core_hours=cost_in_core_sec/3600
cost_in_core_years=cost_in_core_hours/24/365
print (cost_in_core_hours, cost_in_core_years)
```

With this experiment, we get 20.9 core-seconds per special-q, and therefore
we obtain about 2430 core-years for the total sieving time.

## Estimating the linear algebra time (coarsely)

Linear algebra works with MPI. For this section, as well as all linear
algebra-related sections, we assume that you built cado-nfs with MPI
enabled (i.e., the `MPI` shell variable was set to the path of your MPI
installation), and that `CADO_BUILD` points to the directory where the
corresponding binaries were built.

To determine the linear algebra time ahead of time for a sparse binary
matrix with (say) 37M rows/columns and 250 non-zero entries per row, it
is possible to _stage_ a real set-up, just for the purpose of
measurement. Cado-nfs has a useful _staging_ mode precisely for this
purpose. In the RSA-240 context, we advise against its use because of
bugs, but these bugs seem to be less of a hurdle in the DLP-240 case. The
only weirdness is that the random distribution of the generated matrices
seems to be inconsistent with the requested density (as of
[8a72ccdde](https://gitlab.inria.fr/cado-nfs/cado-nfs/commit/8a72ccdde)
at least). To obtain sample timings, one can run the command:
```shell
DATA=$DATA CADO_BUILD=$CADO_BUILD MPI=$MPI nrows=37000000 density=$((250*4/5)) nthreads=32 ./dlp240-linalg-0a-estimate_linalg_time_coarse_method_a.sh
```
(where the multiplication by 4/5 is here to "fix" the issue with the
random distribution). This reports an anticipated time of about 2.65
seconds per iteration (running on 4 nodes of the `grvingt` cluster).

To obtain timings in a different way, the following procedure can also be
used, maybe as a complement to the above, to generate a complete fake
matrix of the required size with the
[`generate_random_matrix.sh`](generate_random_matrix.sh) script (which
takes well over an hour), and measure the time for 128 iterations (which
takes only a few minutes). Within the script
[`dlp240-linalg-0a-estimate_linalg_time_coarse_method_b.sh`](dlp240-linalg-0a-estimate_linalg_time_coarse_method_b.sh),
several implementation-level parameters are set, and should probably be
adjusted to the users' needs.

```shell
DATA=$DATA CADO_BUILD=$CADO_BUILD MPI=$MPI nrows=37000000 density=250 nthreads=32 ./dlp240-linalg-0a-estimate_linalg_time_coarse_method_b.sh
```

This second method reports about 3.1 seconds per iteration. Allowing for
some inaccuracy, these experiments are sufficient to build confidence
that the time per iteration in the Krylov (a.k.a. "sequence") step of
block Wiedemann is close to 3 seconds per iteration, perhaps slightly less.
The time per iteration in the Mksol (a.k.a. "evaluation") step is in the
same ballpark. The time for Krylov+Mksol can then be estimated as the
product of this timing with `(1+n/m+1/n)*N`, with `N` the number of rows,
and `m` and `n` the block Wiedemann parameters (we chose `m=48` and
`n=16`).  Applied to our use case, this gives an anticipated cost of
`(1+n/m+1/n)*N*3*4*32/3600/24/365=628` core-years for Krylov+Mksol (4 and
32 representing the fact that we used 4-node jobs with 32 physical cores
per node).


The "lingen" (linear generator) step of block Wiedemann was seen as
the main potential stumbling block for the computation. We had to ensure
that it would be possible with the resources we had. To this end, a
"tuning" of the lingen program can be done with the `--tune` flag, so as
to get an advance look at the cpu and memory requirements for that step.
These tests were sufficient to convince us that we had several possible
parameter settings to choose from, and that this computation was feasible.

## Validating the claimed sieving results

The benchmark command lines above can be used almost as is to reproduce
the full computation. It is only necessary to remove the `-random-sample`
option and to adjust the `-q0` and `-q1` parameters in order to create
many small work units that in the end cover exactly the global q-range.

Since we do not expect anyone to spend as much computing resources
to perform exactly the same computation again, we provide the count 
of how many (non-unique) relations were produced for each 1G special-q 
sub-range in the [`dlp240-rel_count`](dlp240-rel_count) file.

We can then visually plot this data, as shown in
[`dlp240-plot_rel_count.pdf`](dlp240-plot_rel_count.pdf), where the
x-coordinate denotes the special-q (in multiples of 1G).  The plot is
very regular except for special-q's around 150G and 225G.  The
irregularities in these areas correspond to the beginning of the
computation when we were still adjusting our scripts. We had two
independent servers in charge of distributing sieving tasks, one dealing
with [150G,225G], and the other one dealing with [225G,300G].

In order to validate our computation, it is possible to recompute only
one of the sub-ranges (not one in the irregular areas) and check that the
number of relations is the one we report. This still requires significant
resources. If only a single node is available for the validation, it is
better to rely on sampling as for the estimates in previous sections, and
extrapolate.

## Reproducing the filtering results

All relation files collected during sieving were collated into a more
managable number of large files (150 files of 3.2GB each). These had to
undergo filtering in order to produce a linear system. The process is as
follows.

The filtering follows roughly the same general workflow as in the
[RSA-240 case](../rsa240/filtering.md), with some notable changes:
 - not one, but two programs must be used to generate important companion
   files beforehand:
    ```
    $CADO_BUILD/numbertheory/badideals -poly dlp240.poly -ell 62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651 -badidealinfo $DATA/dlp240.badidealinfo -badideals $DATA/dlp240.badideals
    $CADO_BUILD/sieve/freerel -poly dlp240.poly -renumber $DATA/dlp240.renumber.gz -lpb0 35 -lpb1 35 -out $DATA/dlp240.freerel.gz -badideals $DATA/dlp240.badideals -lcideals -t 32
    ```
 - the command line flags `-dl -badidealinfo $DATA/dlp240.badidealinfo` must be added to the `dup2` program
 - the `merge` and `replay` programs must be replaced by `merge-dl` and
   `replay-dl`, respectively
 - the `replay-dl` command line lists an extra output file
   `dlp240.ideals` that is extremely important for the rest of the
   computation
 - as the linear system to be solved is inhomogenous, another program
   must be called in order to compute the right-hand side of the system.

### Duplicate removal

Duplicate removal was carried out in the default cado-nfs way. We did several
filtering runs as relations kept arriving. For each run, the
integer shell variable `$EXP` was increased by one (starting from 1).
In the command below, `$new_files` is expected to expand to a file
containing a list of file names of new relations (relative to `$DATA`) to
add to the stored set of relations.
```shell
mkdir -p $DATA/dedup/{0..3}
$CADO_BUILD/filter/dup1 -prefix dedup -out $DATA/dedup/ -basepath $DATA -filelist $new_files -n 2 > $DATA/dup1.$EXP.stdout 2> $DATA/dup1.$EXP.stderr
grep '^# slice.*received' $DATA/dup1.$EXP.stderr > $DATA/dup1.$EXP.per_slice.txt
```

This first pass takes about 3 hours on the full data set.
The number of relations per slice is printed by the program and must be
saved for later use (hence the `$DATA/dup1.$EXP.per_slice.txt` file).

The second pass of duplicate removal works independently on each of the
non-overlapping slices. The number of slices can thus be used as a sort
of time-memory tradeoff (here, `-n 2` tells the program to do `2^2=4`
slices).
```shell
for i in {0..3} ; do
    nrels=`awk '/slice '$i' received/ { x+=$5 } END { print x; }' $DATA/dup1.*.per_slice.txt`
    $CADO_BUILD/filter/dup2 -nrels $nrels -renumber $DATA/dlp240.renumber.gz -dl -badidealinfo $DATA/dlp240.badidealinfo $DATA/dedup/$i/dedup*gz > $DATA/dup2.$EXP.$i.stdout 2> $DATA/dup2.$EXP.$i.stderr
done
```
(Note: in newer versions of cado-nfs, after June 2020, the `-badidealinfo
$DATA/dlp240.badidealinfo` arguments to the `dup2` program must be
replaced by `-poly dlp240.poly`.)

### The "purge" step, a.k.a. singleton and "clique" removal

Here is the command line of the last filtering run that we used (revision `492b804fc`), with `EXP=7`:
```shell
nrels=$(awk  '/remaining/ { x+=$4; } END { print x }' $DATA/dup2.$EXP.[0-3].stderr)
colmax=2960421140
$CADO_BUILD/filter/purge -out $DATA/purged$EXP.gz -nrels $nrels -outdel $DATA/relsdel$EXP.gz -keep 3 -col-min-index 0 -col-max-index $colmax -t 56 -required_excess 0.0 $DATA/dedup/*/dedup*gz
```
This took about 7.5 hours on the machine wurst, with 575GB of peak memory.

### The "merge" step
The merge step can be reproduced as follows (still with `EXP=7` for the
final experiment).
```shell
$CADO_BUILD/filter/merge-dl -mat $DATA/purged$EXP.gz -out $DATA/history250_$EXP -target_density 250 -skip 0 -t 28
```
This took about 20 minutes on the machine wurst, with a peak memory of 118GB.

### The "replay" step
Finally the replay step can be reproduced as follows:
```shell
$CADO_BUILD/filter/replay-dl -purged $DATA/purged$EXP.gz -his $DATA/history250_$EXP.gz -out $DATA/dlp240.matrix$EXP.250.bin -index $DATA/dlp240.index$EXP.gz -ideals $DATA/dlp240.ideals$EXP.gz
```

### Computing the right-hand side
This is done with a program called `sm`. There are several variants of this program, and several ways to invoke it. Here is the command line that we used. Note that we use the file `$DATA/dlp240.index$EXP.gz` that was just created by the above step.
```shell
$CADO_BUILD/filter/sm -poly dlp240.poly -purged $DATA/purged$EXP.gz -index $DATA/dlp240.index$EXP.gz -out $DATA/dlp240.matrix$EXP.250.sm -ell 62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651 -t 56
```
This took about four hours on the machine wurst.

## Estimating the linear algebra time more precisely, and choosing parameters

The filtering output is controlled by a wealth of tunable parameters.
However on the very coarse-grain level we focus on two of them:
 * _when_ we decide to stop relation collection.
 * _how dense_ we want the final matrix to be.

Sieving more is expected to have a beneficial impact on the matrix size,
but this benefit can become marginal, eventually reaching a point of diminishing
returns. Allowing a denser matrix also makes it possible
to have fewer rows in the final matrix, which is good for various memory
related concerns.

We did several filtering experiments based on the DLP-240 data set, as
relations kept coming in. For each of these experiments, we give the
number of unique relations, the number of relations after the initial
"purge" step, as well as the number of rows of the final matrix after
"merge", for target densities d=150, d=200, and d=250.

|              | rels  | purged | d=150 | d=200 | d=250
| -------------|-------|--------|-------|-------|------
| experiment 4 | 2.07G |  1.87G | 51.6M | 46.1M | (skip)
| experiment 5 | 2.30G |  1.59G | 45.2M | 40.4M | (skip)
| experiment 6 | 2.38G |  1.52G | 43.0M | 39.0M | (skip)
| experiment 7 | 2.38G |  1.50G | 42.9M | 38.9M | 36.2M

(experiment 7 is basically experiment 6 with a minor tweak: in
`singletons_and_clique_removal()` in `purge.c`, the value of `nsteps` was
doubled, to allow slightly better accuracy with that step.)

Each of these experiments produced a matrix, and it was possible to run a
few iterations of each, in order to guide the final choice. For this,
a single command line is sufficient. For consistency with the other
scripts, it is placed as well in a script in this repository, namely
[`dlp240-linalg-0b-test-few-iterations.sh`](dlp240-linalg-0b-test-few-iterations.sh).
This script needs the `MPI`, `DATA`, `matrix`, and `CADO_BUILD` to be
set. It can be used as follows, where `$matrix` points to one of the
matrices that have been produced by the filter code (after the `replay`
step). For this quick bench, the right-hand-side file is not necessary.
```shell
export matrix=$DATA/dlp240.matrix7.250.bin
export DATA
export CADO_BUILD
export MPI
./dlp240-linalg-0b-test-few-iterations.sh
```

This can be used to decide what density should be preferred (the curve is
usually pretty flat anyway).

With the chosen matrix for the DLP-240 computation (experiment 7, density
250), we observed an average time of roughly 2.6 seconds per iteration
on the `grvingt` platform, subject to some variations (which is somewhat
on the optimistic end of our expected timing range).

## Reproducing the linear algebra results

The input to the linear algebra step consists in four files.
 - The matrix file `$DATA/dlp240.matrix7.250.bin` (69 GB)
 - The companion file `$DATA/dlp240.matrix7.250.rw.bin` (139 MB)
 - The companion file `$DATA/dlp240.matrix7.250.cw.bin` (139 MB)
 - The right-hand side file `$DATA/dlp240.matrix7.250.sm` (33 GB)

This linear system has 36190697 rows and columns.

We decided to use the block Wiedemann parameters `m=48` and `n=16`,
running on 4-node jobs.

The first part of the computation can be done with these scripts:
```shell
export matrix=$DATA/dlp240.matrix7.250.bin
export DATA
export CADO_BUILD
export MPI
./dlp240-linalg-1-prep.sh
./dlp240-linalg-2-secure.sh
./dlp240-linalg-3-krylov.sh sequence=0 start=0
./dlp240-linalg-3-krylov.sh sequence=1 start=0
...
./dlp240-linalg-3-krylov.sh sequence=15 start=0
```
where the last 16 lines (steps `3-krylov`) correspond to the 16 "sequences" (vector blocks
numbered `0-1`, `1-2`, until `15-16`). These sequences can
be run concurrently on different sets of nodes, with no synchronization
needed. Each of these 16 sequences needs about 90 days to complete (in 
practice, we used a different platform than the one we report timings 
for, but the timings and calendar time were in the same ballpark). Jobs 
can be interrupted, and may be restarted exactly
where they were left off. E.g., if the latest of the `V1-2.*` files in
`$DATA` is `V1-2.86016`, then the job for sequence 1 can be restarted
with:
```shell
./dlp240-linalg-3-krylov.sh sequence=1 start=86016
```

Cheap sanity checks can be done periodically with the following script,
which does all of the checks it can do (note that the command is happy if it
finds _no_ check to do as well!)
```shell
export matrix=$DATA/dlp240.matrix7.250.bin
export DATA
export CADO_BUILD
export MPI
./dlp240-linalg-4-check-krylov.sh
```

Once this is done, data must be collated before being processed by the
later steps. After step `5-acollect` below, a file named `A0-16.0-3016704` with
size 240950181888 bytes will be in `$DATA`. Step `6-lingen` below runs on
36 nodes, and completes in approximately one week (periodic
checkpoint/restart is supported).
```shell
export matrix=$DATA/dlp240.matrix7.250.bin
export DATA
export CADO_BUILD
export MPI
./dlp240-linalg-5-acollect.sh
./dlp240-linalg-6-lingen.sh
./dlp240-linalg-7-check-lingen.sh
./dlp240-linalg-8-mksol.sh start=0
./dlp240-linalg-8-mksol.sh start=32768
./dlp240-linalg-8-mksol.sh start=65536
# ... 67 other commands of the same kind (70 in total) ...
./dlp240-linalg-8-mksol.sh start=2260992
./dlp240-linalg-9-finish.sh
```

All steps `8-mksol.sh` above can be run in parallel (they use the `V*`
files produced in steps `3-krylov` above as a means to jump-start the
computation in the middle). Each uses 8 nodes and takes about 13 hours to
complete (1.43 seconds per iteration). Note that in order to bench the
Mksol timings ahead of time, it is possible to create fake files with
random data, named as follows
```
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.0-1
-rw-rw-r-- 1 ethome users       104 Nov  3  2019 F.sols0-1.0-1.rhs
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.1-2
-rw-rw-r-- 1 ethome users       104 Nov  3  2019 F.sols0-1.1-2.rhs
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.10-11
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.11-12
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.12-13
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.13-14
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.14-15
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.15-16
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.2-3
-rw-rw-r-- 1 ethome users       104 Nov  3  2019 F.sols0-1.2-3.rhs
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.3-4
-rw-rw-r-- 1 ethome users       104 Nov  3  2019 F.sols0-1.3-4.rhs
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.4-5
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.5-6
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.6-7
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.7-8
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.8-9
-rw-rw-r-- 1 ethome users 235239680 Nov  3  2019 F.sols0-1.9-10
```
(the size above is the final size. For a quick test, it is sufficient to
replace the big files with files of size
`13*8*32768=3407872` bytes.)

After having successfully followed the steps above, a file named
`K.sols0-1.0.txt` will be in `$DATA`. This file represents a nullspace
vector for the matrix (M|RHS) (where M has dimension 36190697x36190693).

## Back-substituting the linear algebra result in collected relations

The linear algebra result can be understood as the "core" of the
solutions. It is important to back-substitute this data into all
relations that were collected during the relation collection phase, in
order to have a database of known logarithms that can be used for
individual logarithm computations.

This is achieved by the `reconstructlog-dl` program.

```shell
$CADO_BUILD/filter/reconstructlog-dl -ell 62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651 -mt 28 -log $DATA/K.sols0-1.0.txt -out $DATA/dlp240.reconstructlog.dlog -renumber $DATA/dlp240.renumber.gz -poly dlp240.poly -purged $DATA/purged7.gz -ideals $DATA/p240.ideals7.gz -relsdel $DATA/relsdel7.gz -nrels 2380725637
```

As written, this command line takes an annoyingly large amount of time
(several weeks). It is possible to reduce this time by precomputing
uncompressed files `$DATA/purged7_withsm.txt` and `$DATA/relsdel7_withsm.txt` that have the Schirokauer maps already computed. These can be computed well ahead of time with the `sm_append` program, which also works with MPI and scales very well. An example command line is:
```shell
$MPI/bin/mpiexec [[your favorite mpiexec args]] $CADO_BUILD/filter/sm_append -ell 62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651 -poly $HERE/p240.poly -b 4096     -in "/grvingt/zimmerma/dlp240/filter/purged7.gz" -out "${HERE}/purged7.withsm.txt"
```
We did this in 8 hours on 16 grvingt nodes. Note that the files
`$DATA/purged7_withsm.txt` and `$DATA/relsdel7_withsm.txt`  are quite
big: 158GB and 2.3TB, respectively.

Once this precomputation is done, the two big files can be used as
drop-in replacements to the corresponding files in the
`reconstructlog-dl` command line, and the program completes in about two
days.

## Reproducing the individual logarithm result

In principle the general script `scripts/descent.py` in the `cado-nfs`
distribution can perform all the required steps for performing an
individual logarithm computation. However, this script has not been
optimized for large computations, and in particular it starts by reading
the whole database of known discrete logarithms in central memory which
is slow and required a machine with a huge amount of memory.

In the file [`howto-descent.md`](howto-descent.md), we explain what we did to make our lives
simpler with this step. We do not claim full reproducibility here, since
this is admittedly hacky (we also give a small C program that searches
in the database file without having an in-memory image). In any case,
this step cannot be done without the database file
`dlp240.reconstructlog.dlog` that is too large (465 GB) to put in this
repository.

