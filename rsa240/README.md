# RSA-240

This repository contains information to reproduce the RSA-240 factoring record.

There are several subsections.

* [Software prerequisites, and reference hardware configuration](#software-prerequisites-and-reference-hardware-configuration)
* [Searching for a polynomial pair](#searching-for-a-polynomial-pair)
* [Estimating the number of (unique) relations](#estimating-the-number-of-unique-relations)
* [Estimating the cost of sieving](#estimating-the-cost-of-sieving)
* [Estimating the linear algebra time (coarsely)](#estimating-the-linear-algebra-time-coarsely)
* [Validating the claimed sieving results](#validating-the-claimed-sieving-results)
* [Reproducing the filtering results](#reproducing-the-filtering-results)
* [Estimating the linear algebra time more precisely, and choosing parameters](#estimating-the-linear-algebra-time-more-precisely-and-choosing-parameters)
* [Reproducing the linear algebra results](#reproducing-the-linear-algebra-results)

## Software prerequisites, and reference hardware configuration

This documentation relies on [commit 8a72ccdde of
cado-nfs](https://gitlab.inria.fr/cado-nfs/cado-nfs/commit/8a72ccdde) as a
baseline. In some cases we provide exact commit numbers for specific
commands as well.

The cado-nfs documentation should be followed, in order to obtain a
complete build.  Note in particular that some of the experiments below
require the use of the [hwloc](https://www.open-mpi.org/projects/hwloc/)
library, as well as an MPI implementation. [Open
MPI](https://www.open-mpi.org/) is routinely used for tests, but cado-nfs
also works with Intel MPI, for instance. The bottom line is that although
these external pieces of software are marked as _optional_ for cado-nfs,
they must be regarded as real prerequisites for large experiments.

Hardware-wise, the vast majority of the data collected in this repository
was collected on a cluster called `grvingt`. The hardware characteristics
of the cluster are as follows.
 * Dell Poweredge C6420 (installed 2018/07)
 * 64 nodes, form factor 1/2 U (4 nodes in a 2U enclosure).
 * 2 Intel Xeon Gold 6130 processors per node (2.1GHz)
 * 192GB DDR4 RAM, 2666 MT/s (12 DIMM, 16GB each).

Several hardware description files are available in this directory, in
particular the output of the [`lstopo --of xml`](lstopo.grvingt.xml) and
[`dmidecode`](dmidecode.grvingt.out) commands.

Some memory intensive tasks were performed on a dedicated machine called
`wurst`. The hardware characteristics are as follows.
 * Dell Poweredge R930 (installed 2015/11)
 * form factor 4 U
 * 4 Intel(R) Xeon(R) CPU E7-4850 v3 @ 2.20GHz
 * 1536GB DDR4 RAM, 2133 MT/s (48 DIMM, 32GB each).

As above, we provide the output of the [`lstopo --of
xml`](lstopo.wurst.xml) and [`dmidecode`](dmidecode.wurst.out) commands.

With respect to the compilation of cado-nfs, the user-level configuration is
in a file called `local.sh`. An [example `local.sh` file](local.sh)
is provided, and you should probably adjust it to your needs. In all our
experiments, cado-nfs was compiled with up-to-date software (updated
debian 9 or debian 10). Typical software used were the GNU C compilers
versions 6 to 9, or Open MPI versions 4.0.1 to 4.0.3.

Most (if not all) information boxes in this document rely on two shell
variables, `CADO_BUILD` and `DATA`, be set and exported to shell
subprocess (as with `export CADO_BUILD=/blah/... ; export
DATA=/foo/...`). The `CADO_BUILD` variable is assumed to be the path to
a successful cado-nfs build directory. The `DATA` variable, which is
used by some scripts, should point to a directory with plenty of storage,
possibly on some shared filesystem. Storage is also needed to store the
temporary files with collected relations. Overall, a full reproduction of
the computation would need in the vicinity of 10TB of storage. All
scripts provided in this document expect to be run from the directory where
they are placed, since they are also trying to access companion data
files.

There is a considerable amount of biodiversity in the possible computing
environments in HPC centers. You might encounter difficulties in
matching the idiosyncrasies of your particular job scheduler to the way
cado-nfs expects to use the hardware. As a general rule, pay attention to
the fact that all the important steps of NFS in cado-nfs use all
available cores, and jobs expect to have exclusive control of the nodes
they run on. Any situation where you notice that this does not happen
is a hint at the fact that something has gone wrong. We cannot possibly
provide guidance for every possible setup.

## Searching for a polynomial pair

See the [`polyselect.md`](polyselect.md) file in this repository.

The winner is the [`rsa240.poly`](rsa240.poly) file:

```shell
cat > rsa240.poly <<EOF
n: 124620366781718784065835044608106590434820374651678805754818788883289666801188210855036039570272508747509864768438458621054865537970253930571891217684318286362846948405301614416430468066875699415246993185704183030512549594371372159029236099
poly0: -105487753732969860223795041295860517380,17780390513045005995253
poly1: -221175588842299117590564542609977016567191860,1595712553369335430496125795083146688523,179200573533665721310210640738061170,974448934853864807690675067037,-6381744461279867941961670,-4763683724115259920,10853204947200
skew: 209090.135
# lognorm 66.19, E 57.71, alpha -8.48 (proj -3.18), 6 real roots
# MurphyE(Bf=2.100e+09,Bg=1.800e+09,area=2.400e+19)=7.41e-13
EOF
```

## Estimating the number of (unique) relations

To estimate the number of relations produced by a set of parameters:
- We compute the corresponding factor bases. In our case, side 0 is
  rational, so there is no need to precompute the factor base. (Note that
  this precomputed factor base is different from what we call the "factor
  base cache", and which also appears later.)
- We create a "hint" file where we tell which strategy to use for which
  special-q size.
- We random sample in the global q-range, using sieving and not batch:
  this produces the same relations. This is slower but `-batch` is
  incompatible (with the version of cado-nfs we used) with on-the-fly duplicate removal.

Here is the result with the parameters that were used in the computation.

The computation of the factor base is done with the following command.
Here, `-t 16` specifies the number of threads (more is practically
useless, since gzip soon becomes the limiting factor).

```shell
$CADO_BUILD/sieve/makefb -poly rsa240.poly -side 1 -lim 2100000000 -maxbits 16 -t 16 -out $DATA/rsa240.fb1.gz
```

The file has size 786781774 bytes, and takes less than 4 minutes
to compute on the `grvingt` computers.


The hint file is [`rsa240.hint`](rsa240.hint), and has a weird format.
The following basically says "Three algebraic large primes for special-q's
less than 2^31, and two otherwise."

```shell
cat > rsa240.hint <<EOF
30@1 1.0 1.0 A=32 1800000000,36,72,2.2 2100000000,37,111,3.2
31@1 1.0 1.0 A=32 1800000000,36,72,2.2 2100000000,37,111,3.2
32@1 1.0 1.0 A=32 1800000000,36,72,2.2 2100000000,37,74,2.2
33@1 1.0 1.0 A=32 1800000000,36,72,2.2 2100000000,37,74,2.2
EOF
```

We can now sieve for random-sampled special-q's, and remove duplicate
relations on the fly. In the output of the command line below, only the
number of unique relations per special-q matters. The timing does not
matter.

```shell
$CADO_BUILD/sieve/las -poly rsa240.poly -fb1 $DATA/rsa240.fb1.gz -lim0 1800000000 -lim1 2100000000 -lpb0 36 -lpb1 37 -q0 8e8 -q1 7.4e9 -dup -dup-qmin 0,800000000 -sqside 1 -A 32 -mfb0 72 -mfb1 111 -lambda0 2.2 -lambda1 3.2 -random-sample 1024 -t auto -bkmult 1,1l:1.15,1s:1.4,2s:1.1 -v -bkthresh1 90000000 -adjust-strategy 2 -fbc /tmp/rsa240.fbc -hint-table rsa240.hint
```

In slightly more than an hour on our target machine `grvingt`, this gives
the estimate that 19.6 unique relations per special-q can be expected
based on these parameters. (Note that this test can also be done in
parallel over several nodes, using the `-seed [[seed value]]` argument in
order to vary the random picks.)

In order to derive an estimate of the total number of (de-duplicated)
relations, it is necessary to multiply the average number of relations per
special-q as obtained during the sample sieving by the number of
special-q's in the global q-range. The latter can be precisely estimated
using the logarithmic integral function as an approximation of the number
of degree-1 prime ideals below a bound. In
[Sagemath](https://www.sagemath.org/) code, this gives:

```python
# [sage]
ave_rel_per_sq = 19.6   ## pick value output by las
number_of_sq = log_integral(7.4e9) - log_integral(8e8)
tot_rels = ave_rel_per_sq * number_of_sq
print (tot_rels)
# 5.88556387364565e9
```
This estimate (5.9G relations) can be made more precise by increasing the
number of special-q's that are sampled for sieving. It is also possible to
have different nodes sample different sub-ranges of the global range to
get the result faster. Sampling 1024 special-q's should be
enough to get a reliable estimate.

## Estimating the cost of sieving

The sieving step requires a lot of computing power and therefore many
different machines were used during the record computation. It is
interesting to normalize the cost for a single typical machine, for which
we do measurements "under lab conditions":
- no known hardware or software issues;
- appropriate cooling so that the processors can use their top frequency;
- no other job running at the same time, apart from standard OS
  functioning;
- no slow disk access due to competing I/Os.

On the `grvingt` cluster that we use for the measurement, these perfect
conditions were reached during production most of the time as well.

In production there is no need to activate the on-the-fly duplicate
removal, which is supposed to be cheap but maybe not negligible. There is
also no need to pass in the hint file, since we are going to run the siever
on different parts of the q-range, and the parameters are
constant on each of them. Finally, during a benchmark, it is important to emulate the
fact that the cached factor base (the `/tmp/rsa240.fbc` file) is
precomputed and hot (i.e., cached in memory by the OS and/or the
hard-drive), because this is the situation in production; for this, it
suffices to start a first run and interrupt it as soon as the cache is
written (or pass `-nq 0`).

#### Cost of 2-sided sieving in the q-range [8e8,2.1e9]

In order to measure the cost of sieving in the special-q subrange where
sieving is used on both sides, the typical command line is as follows:

```shell
time $CADO_BUILD/sieve/las -poly rsa240.poly -fb1 $DATA/rsa240.fb1.gz -lim0 1800000000 -lim1 2100000000 -lpb0 36 -lpb1 37 -q0 8e8 -q1 2.1e9 -sqside 1 -A 32 -mfb0 72 -mfb1 111 -lambda0 2.2 -lambda1 3.2 -random-sample 1024 -t auto -bkmult 1,1l:1.15,1s:1.4,2s:1.1 -v -bkthresh1 90000000 -adjust-strategy 2 -fbc /tmp/rsa240.fbc
```

(note: the first time this command line is run, it takes some time to
create the "cache" file `/tmp/rsa240.fbc`. If you want to avoid this, you
may run the command with `-random-sample 1024` replaced by
`-random-sample 0` first, which will _only_ create the cache file. Then
run the command above.)

While `las` tries to print some running times, some startup or finish
tasks might be skipped; furthermore the CPU time becomes easily confused by
the hyperthreading. Therefore, it is better to rely on `time`, since this
gives the real wall-clock time exactly as it was taken by the
computation.

On our sample machine `grvingt`, the result of the above line is:
```
real	75m54.351s
user	4768m41.853s
sys	43m15.877s
```
Then the `75m54.351s=4554.3s` value must be appropriately scaled in order
to convert it into physical core-seconds. For instance, in our case,
since there are 32 physical cores and we sieved 1024 special-q's, this
gives `4554.3*32/1024=142.32` core.seconds per special-q.

Finally, we need to to multiply by the number of special-q's in this
subrange. We get (in Sagemath):

```python
# [sage]
cost_in_core_sec=(log_integral(2.1e9)-log_integral(8e8))*4554.3*32/1024
cost_in_core_hours=cost_in_core_sec/3600
cost_in_core_years=cost_in_core_hours/24/365
print (cost_in_core_hours, cost_in_core_years)
```

With this experiment, we estimate about 279 core.years for this sub-range.


#### Cost of 1-sided sieving + batch in the q-range [2.1e9,7.4e9]

For special-q's larger then 2.1e9, since we are using batch smoothness
detection on side 0, we have to precompute the `rsa240.batch0` file which
contains the product of all primes to be extracted. (Note that the
`-batch1` option is mandatory, even if for our parameters, no file is
produced on side 1.)

```shell
$CADO_BUILD/sieve/ecm/precompbatch -poly rsa240.poly -lim0 0 -lim1 2100000000 -batch0 $DATA/rsa240.batch0 -batch1 $DATA/rsa240.batch1 -batchlpb0 31 -batchlpb1 30
```

Then, we can use the [`rsa240-sieve-batch.sh`](rsa240-sieve-batch.sh)
shell script given in this repository. This launches:
- one instance of `las`, which does the sieving on side 1 and prints the
  survivors to files;
- 6 instances of the `finishbatch` program. Those instances process the
  files as they are produced, do the batch smoothness detection, and
  produce relations.

The script takes two command line arguments `-q0 xxx` and `-q1 xxx`,
which describe the range of special-q to process. Temporary files are put
in the `/tmp` directory by default.

In order to run [`rsa240-sieve-batch.sh`](rsa240-sieve-batch.sh) on your
own machine, there are some variables to adjust at the beginning of the
script. Two examples are already given, so this should be easy to
imitate. The number of instances of `finishbatch` can also be adjusted
depending on the number of cores available on the machine.

When the paths are properly set (either by having `CADO_BUILD` and
`DATA` set correctly, or by tweaking the script), a typical invocation
is as follows:
```shell
./rsa240-sieve-batch.sh -q0 2100000000 -q1 2100100000
```
The script prints the start and end date on stdout. The number
of special-q's that have been processed can be found in the output of
`las`, which is written to `$DATA/log/las.${q0}-${q1}.out`. One can again 
deduce the cost in core-seconds to process one special-q from this 
information, and then the overall cost of sieving the q-range [2.1e9,7.4e9].

The design of this script imposes a rather long range of
special-q's to handle for each run of `rsa240-sieve-batch.sh`. Indeed,
during the final minutes, the `finishbatch` jobs need to take care of the
last survivor files while `las` is no longer running, so the node is
not fully occupied. If the `rsa240-sieve-batch.sh` job takes a few hours,
this fade-out phase takes negligible time. Both for the benchmark and in
production it is thus necessary for the jobs to take at least a few hours.

On our sample machine, here is an example of a benchmark:
```shell
./rsa240-sieve-batch.sh -q0 2100000000 -q1 2100100000 > /tmp/rsa240-sieve-batch.out

# [ wait ... ]

start=$(date -d "`grep "^Starting" /tmp/rsa240-sieve-batch.out | head -1 | awk -F " at " '//{print $2}'`" +%s)
end=$(date -d "`grep "^End" /tmp/rsa240-sieve-batch.out | tail -1 | awk -F " at " '//{print $2}'`" +%s)
nb_q=`grep "# Discarded 0 special-q's out of" /tmp/log/las.2100000000-2100100000.out | awk '{print $(NF-1)}'`
echo -n "Cost in core.sec per special-q: "; echo "($end-$start)/$nb_q*32" | bc -l
# Cost in core.sec per special-q: 67.43915571828559121248
```

```python
# [sage]
cost_in_core_sec=(log_integral(7.4e9)-log_integral(2.1e9))*67.4
cost_in_core_hours=cost_in_core_sec/3600
cost_in_core_years=cost_in_core_hours/24/365
print (cost_in_core_hours, cost_in_core_years)
# (4.46604511452076e6, 509.822501657621)
```

With this experiment, we get 67.4 core.sec per special-q, and therefore
we obtain about 510 core.years for this sub-range.

## Estimating the linear algebra time (coarsely)

Linear algebra works with MPI. For this section, as well as all linear
algebra-related sections, we assume that you have built cado-nfs with MPI
enabled (i.e., the `MPI` shell variable was set to the path of your MPI
installation), and that `CADO_BUILD` points to the directory where the
corresponding binaries were built.

The matrix size for RSA-240 is about 282M, with density 200 per row.
However, it is possible, and actually useful, to have an idea of the
computational cost of linear algebra before the matrix is actually ready,
just based on a rough prediction of its size. Tools that we developed for
simulating the filtering, although quite fragile, can be used for this purpose. 
See the script
[`scripts/estimate_matsize.sh`](https://gitlab.inria.fr/cado-nfs/cado-nfs/-/blob/8a72ccdde/scripts/estimate_matsize.sh)
available in the cado-nfs repository, for example.

As an illustration of how it is possible to determine the linear algebra
cost ahead of time, let us assume that some advance prediction tells us
that the expected size of the sparse binary matrix is (say) 300M
rows/columns and 200 non-zero entries per row. (As we know after the
fact, this is not too far from reality, but the whole point is that the
_real_ matrix is not known at this point, of course.) Based on these
characteristics, it is possible to _stage_ a real set-up, just for the
purpose of measurement. cado-nfs has a useful _staging_ mode precisely
for that purpose, but it seems to misbehave (as of commit
[8a72ccdde](https://gitlab.inria.fr/cado-nfs/cado-nfs/commit/8a72ccdde)
at least), and cannot be used (see the script
[`rsa240-linalg-0a-estimate_linalg_time_coarse_method_a.sh`](rsa240-linalg-0a-estimate_linalg_time_coarse_method_a.sh)
for how it "should" work, though).

To obtain reasonably accurate timings, the following procedure can be
used to generate a complete fake matrix of the required size with the
[`generate_random_matrix.sh`](generate_random_matrix.sh) script (which
takes well over an hour), and measure the time for 128 iterations (which
takes only a few minutes). Within the script
[`rsa240-linalg-0a-estimate_linalg_time_coarse_method_b.sh`](rsa240-linalg-0a-estimate_linalg_time_coarse_method_b.sh),
several implementation-level parameters are set, and should probably be
adjusted to the users' needs. Along with the `DATA` and `CADO_BUILD`
variables, the script below also requires the `MPI` shell variable to
be set and exported, so that `$MPI/bin/mpiexec` can actually run MPI
programs. In all likelihood, this script needs to be tweaked depending on
the specifics of how MPI programs should be run on the target platform.
```shell
nrows=300000000 density=200 nthreads=32 ./rsa240-linalg-0a-estimate_linalg_time_coarse_method_b.sh
```

This reports about 1.3 seconds per iteration. Allowing for some
inaccuracy, this experiment is sufficient to build confidence that the
time per iteration in the Krylov (a.k.a. "sequence") step of block
Wiedemann is about 1.2 to 1.5 seconds per iteration (handling 64-bit wide
vectors). The time per iteration in the Mksol (a.k.a. "evaluation") step
is in the same ballpark. The time for Krylov+Mksol can then be estimated
as the product of this timing with `(1+n/m+64/n)*(N/64)`, with `N` the
number of rows, and `m` and `n` the block Wiedemann parameters (we chose
`m=512` and `n=256`).  Applied to our use case, this gives an anticipated
cost of `(1+n/m+64/n)*(N/64)*1.3*8*32/3600/24/365=86.6` core-years for
Krylov+Mksol (8 and 32 representing the fact that we used 8-node jobs
with 32 physical cores per node).

Because the parallel code for the "lingen" (linear generator) step of
block Wiedemann was not ready when the computation started, we made no
attempt to anticipate the timing. We were confident that the cost was
going to be small in any case.

## Validating the claimed sieving results

The benchmark command lines above can be used almost as is to reproduce
the full computation. It is only necessary to remove the `-random-sample`
option and to adjust the `-q0` and `-q1` to create many small work units
that in the end cover exactly the global q-range.

Since we do not expect anyone to spend as many computing resources
to perform exactly the same computation again, we provide the count 
of how many (non-unique) relations were produced for each 100M special-q 
sub-range in the [`rsa240-rel_count`](rsa240-rel_count) file . 

We can then plot this data visually, as shown in
[`rsa240-plot_rel_count.pdf`](rsa240-plot_rel_count.pdf) where we see the
drop in the number of relations produced per special-q when changing the
strategy (the x-coordinate is the special-q value, divided by 100M).  The
plot is very regular on the two sub-ranges, except for outliers
corresponding to the q-range [9e8,1e9].  This is due to a crash of a
computing facility, and it was easier to allow duplicate computations
(hence duplicate relations) than to sort out exactly which special-q's
did not complete.

In order to validate our computation, it is possible to re-compute only
one of the sub-ranges (except 9e8-1e9) and check that the number of
relations is the one we report. This still requires significant
resources. If only a single node is available for the validation, it is
better to rely on sampling as for the estimates in previous sections, and
extrapolate.

## Reproducing the filtering results

The filtering step in cado-nfs proceeds through several steps.
 - duplicate removal.
 - "purge", a.k.a. singleton and "clique" removal; also sometimes
   referred to as only "filtering".
 - "merge", which computes a sequence of row combinations.
 - "replay", which replays the above sequence to produce a small matrix.

The file [`filtering.md`](filtering.md) in this repository gives more
information on these steps.

## Estimating the linear algebra time more precisely, and choosing parameters

The filtering output is controlled by a wealth of tunable parameters.
However on the very coarse-grain level we focus on two of them:
 * _when_ we decide to stop relation collection,
 * _how dense_ we want the final matrix to be.

Sieving more is expected to have a beneficial impact on the matrix size,
but this benefit can become marginal, reaching a point of diminishing
returns eventually. Allowing for a denser matrix also makes it possible
to have fewer rows in the final matrix, which is good for various memory
related concerns.

We did several filtering experiments based on the RSA-240 data set, as
relations kept coming in. For each of these experiments, we give the
number of unique relations, the number of relations after the initial
"pruning" step of filtering (called "purge" in cado-nfs), as well as the
number of rows of the final matrix after "merge", for target densities
d=100, d=150, and d=200.

|               | rels  | purged | d=100 | d=150 | d=200
| --------------|-------|--------|-------|-------|------
| experiment 9  | 5.70G |  1.36G |  409M |  345M |  312M
| experiment 10 | 5.81G |  1.29G |  386M |  331M |  299M
| experiment 11 | 6.01G |  1.18G |  363M |  310M |  282M

Each of these experiments produced a matrix, and it was possible to run a
few iterations of each, in order to guide the final choice. For this,
a single command line is sufficient. For consistency with the other
scripts, it is placed as well in a script in this repository, namely
[`rsa240-linalg-0b-test-few-iterations.sh`](rsa240-linalg-0b-test-few-iterations.sh).
This script needs the `MPI`, `DATA`, `matrix`, and `CADO_BUILD` environment variables to be
set. It can be used as follows, where `$matrix` points to one of the
matrices that have been produced by the filter code (after the `replay`
step).
```shell
export matrix=$DATA/rsa240.matrix11.200.bin
export DATA
export CADO_BUILD
export MPI
./rsa240-linalg-0b-test-few-iterations.sh
```

This can be used to decide what density should be preferred (the curve is
usually pretty flat anyway).

With the chosen matrix for the RSA-240 computation (experiment 11,
density 200), we observed an average time of roughly 1.32 seconds per
iteration on the `grvingt` platform, subject to some variations.

## Reproducing the linear algebra results

The scripts above are of course part of a more general picture that runs
the full block Wiedemann algorithm.

We decided to use the block Wiedemann parameters `m=512` and `n=256`,
giving rise to `n/64=4` sequences to be computed indepedently. We used
8-node jobs.

The first part of the computation can be done with these scripts:
```shell
export matrix=$DATA/rsa240.matrix11.200.bin
export DATA
export CADO_BUILD
export MPI
./rsa240-linalg-1-prep.sh
./rsa240-linalg-2-secure.sh
./rsa240-linalg-3-krylov.sh sequence=0 start=0
./rsa240-linalg-3-krylov.sh sequence=1 start=0
./rsa240-linalg-3-krylov.sh sequence=2 start=0
./rsa240-linalg-3-krylov.sh sequence=3 start=0
```
where the last 4 lines (steps `3-krylov`) correspond to the 4 "sequences"
(vector blocks numbered `0-64`, `64-128`, `128-192`, and `192-256`).
These sequences can be run concurrently on different sets of nodes, with
no synchronization needed. Each of these 4 sequences needs about 25 days
to complete. Jobs can be interrupted, and can simply be restarted
exactly from the point where they were left off. E.g., if the latest of the `V64-128.*`
files in `$DATA` is `V64-128.86016`, then the job for sequence 1 can be
restarted with:
```shell
./rsa240-linalg-3-krylov.sh sequence=1 start=86016
```

Cheap sanity checks can be done periodically with the following script,
which does all of the checks it can do (note that the command is happy if it
finds _no_ check to do as well!)
```shell
export matrix=$DATA/rsa240.matrix11.200.bin
export DATA
export CADO_BUILD
export MPI
./rsa240-linalg-4-check-krylov.sh
```

Once this is finished, data must be collated before being processed by the
later steps. After step `5-acollect` below, a file named
`A0-256.0-1654784` with size 27111981056 bytes will be in `$DATA`. Step
`6-lingen` below runs on 16 nodes, and completes in slightly less than 10
hours.
```shell
export matrix=$DATA/rsa240.matrix11.200.bin
export DATA
export CADO_BUILD
export MPI
./rsa240-linalg-5-acollect.sh
./rsa240-linalg-6-lingen.sh
# there is no step 7, for consistency with the discrete log case.
./rsa240-linalg-8-mksol.sh start=0
./rsa240-linalg-8-mksol.sh start=32768
./rsa240-linalg-8-mksol.sh start=65536
# ... 31 other commands of the same kind (34 in total) ...
./rsa240-linalg-8-mksol.sh start=1081344
./rsa240-linalg-9-finish.sh
```

All steps `8-mksol.sh` above can be run in parallel (they use the `V*`
files produced in steps `3-krylov` above as a means to jump-start the
computation in the middle). Each uses 8 nodes and takes about 13 hours to
complete (1.43 seconds per iteration). Note that in order to benchmark the
Mksol timings ahead of time, it is possible to create fake files named as
follows
```
-rw-r--r-- 1 ethome users 564674048 Nov 20 21:47 F.sols0-64.0-64
-rw-r--r-- 1 ethome users 564674048 Nov 20 21:47 F.sols0-64.128-192
-rw-r--r-- 1 ethome users 564674048 Nov 20 21:47 F.sols0-64.192-256
-rw-r--r-- 1 ethome users 564674048 Nov 20 21:47 F.sols0-64.64-128
```
(the size above is the final size. For a quick test, a size of
`64*64/8*32768=16777216` bytes would be enough.)

After the steps above have been successfully followed, a file named
`W.sols0-64` will be in `$DATA` (with a symlink to it called `W`). This
file represents a kernel vector.

## Reproducing the characters step

Let `W` be the kernel vector computed by the linear algebra step.
The characters step transforms this kernel vector into dependencies.
We used the following command on the machine `wurst`:
```shell
$CADO_BUILD/linalg/characters -poly rsa240.poly -purged $DATA/purged11.gz -index $DATA/rsa240.index11.gz -heavyblock $DATA/rsa240.matrix11.200.dense.bin -out $DATA/rsa240.kernel -ker $DATA/W -lpb0 36 -lpb1 37 -nchar 50 -t 56
```
This gave 21 dependencies after a little more than one hour
(`rsa240.dep.000.gz` to `rsa240.dep.020.gz`).

## Reproducing the square root step

The following command line can be used to process dependencies `start` to
`start+t-1`, using `t` threads (one thread for each dependency):
```shell
$CADO_BUILD/sqrt/sqrt -poly rsa240.poly -prefix $DATA/rsa240.dep.gz -side0 -side1 -gcd -dep $start -t $t
```
The `stdout` file contains one line per dependency, either FAIL or a
non-trivial factor of RSA-240.
