#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD matrix MPI DATA ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in prep ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 8
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    --mca mtl ofi
    --mca mtl_ofi_prov psm2
    --mca btl '^openib'
)
cargs=(
    m=512 n=256 prime=2
    mpi=4x2 thr=2x32
    sequential_cache_build=16
    sequential_cache_read=2
    wdir=$DATA
    matrix="$matrix"
    cpubinding="Package=>2x1 L2Cache*16=>1x32 PU*2=>1x1"
)

cbase=$(basename $matrix .bin).8x64
cdir=$DATA/$cbase
cache_files=()
if [ -d "$cdir" ] ; then cache_files=(`find $cdir/ -name $cbase.*.bin`) ; fi
if [ "${#cache_files[@]}" != 512 ] ; then
    # regenerate caches. this tends to have a negative effect on the
    # runtime, so we'll do _just_ that at first, and restart the full
    # process afterwards.
    for f in "${matrix%bin}"{rw,cw}".bin" ; do
        if ! (cd "$DATA" ; [ -f "$f" ]) ; then echo "$f must exist" >&2 ; exit 1 ; fi
    done
    "${mpi[@]}" $CADO_BUILD/linalg/bwc/dispatch "${cargs[@]}" ys=0..64
fi
"${mpi[@]}" $CADO_BUILD/linalg/bwc/prep "${cargs[@]}"
