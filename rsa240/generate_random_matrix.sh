#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD matrixname ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in random_matrix mf_scan2 ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

: ${nrows=280000000}
: ${density=200}
: ${nthreads=32}
: ${mf_nthreads=4}
: ${dlp=false}

echo "Generating a random matrix with $nrows rows and columns, and $density non-zero coefficients per row on average. Generation done with $nthreads chunks in parallel"
chunkdir=`mktemp -d $matrixname.chunks.XXXXXXXXXXXXXX`
trap "rm -rf $chunkdir" EXIT
for i in `seq 1 $nthreads` ; do
    aa=()
    if [ "$dlp" = true ] ; then
        aa+=(-c 32)
    fi
    $CADO_BUILD/linalg/bwc/random_matrix $((nrows/nthreads)) $nrows -d $density --kleft $((256/$nthreads)) --binary -o $chunkdir/chunk$i.bin -s $i "${aa[@]}" &
done
wait
echo "Done generating chunks. Now concatenating them to $matrixname"
for i in `seq 1 $nthreads` ; do
    f=$chunkdir/chunk$i.bin
    cat $f >&3
    echo "Done reading chunk $i/$nthreads"
done 3> $matrixname
echo "Extra reading pass to compute row and column weights"
aa=()
if [ "$dlp" = true ] ; then
    aa+=(--withcoeffs)
fi
$CADO_BUILD/linalg/bwc/mf_scan2 --threads 4 $matrixname -thread-private-count 1000000 "${aa[@]}"
echo "Done generating $matrixname"
