This repository contains information to reproduce the RSA-240, DLP-240 and RSA-250 records.
 * [RSA-240](rsa240/README.md)
 * [DLP-240](dlp240/README.md)
 * [RSA-250](rsa250/README.md)

This repository is a companion to the following paper, by Fabrice Boudot,
Pierrick Gaudry, Aurore Guillevic, Nadia Heninger, Emmanuel Thomé, and
Paul Zimmermann.

F. Boudot et al. &ldquo;Comparing the Difficulty of Factorization and Discrete Logarithm: A 240-Digit Experiment&rdquo;. In: _CRYPTO 2020, Part II_. Ed. by Daniele Micciancio and Thomas Ristenpart. Vol. 12171. LNCS.
Springer, Heidelberg, Aug. 2020, pp. 62&ndash;91. doi: <a href="http://doi.org/10.1007/978-3-030-56880-1_3">10.1007/978-3-030-56880-1_3</a>. Other links:
<a href="http://ia.cr/2020/697">Eprint</a>;
<a href="https://hal.inria.fr/hal-02863525/">HAL</a>.
