#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD MPI DATA ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in lingen_u64k1 ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 16
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    # https://github.com/open-mpi/ompi/issues/7058 -- still
    # no reliable way to use ofi
    --mca mtl ^psm2,ofi,cm
    --mca btl '^openib'
)
args=(
    m=512 n=256 prime=2
    mpi=4x4
    wdir=$DATA
    --afile A0-256.0-1654784
    --ffile F
    --split-output-file 1
    tuning_timing_cache_filename="`dirname $0`"/rsa240-linalg-6-lingen-tim240.txt
    tuning_schedule_filename="`dirname $0`"/rsa240-linalg-6-lingen-ts240.txt
    max_ram=192
    # thr=8x4 (do not specify thr)
    tree_stats_max_nesting=3
    basecase_keep_until=1.1
    checkpoint_directory=cp
    checkpoint_threshold=200000
    logline_threshold=4096,262144
    logline_timings=1
)
"${mpi[@]}" $CADO_BUILD/linalg/bwc/lingen_u64k1 "${args[@]}"
