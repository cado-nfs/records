#!/bin/bash

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD DATA ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in lingen_verify_checkpoints_p_13 ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

# This is not an MPI program, but we need to know the MPI setting in
# order to properly detect files that were saved in multiple pieces.
args=(
    m=48 n=16
    prime=62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651
    mpi=4x4
    wdir=$DATA/cp
)
$CADO_BUILD/linalg/bwc/lingen_verify_checkpoints_p_13 "${args[@]}"
