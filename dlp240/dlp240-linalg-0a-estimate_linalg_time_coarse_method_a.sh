#!/bin/bash

# This script is buggy, as of commit 8a72ccdde in cado-nfs.
# There are two problems.

# * The locally generated random matrices do not fit the claimed
#   distribution.
#
#   For a matrix with 37M rows/cols and expected density 200, generated
#   over 32*8 threads, it seems that the "density" parameters must be
#   multiplied by the ratio 4/5=0.8, i.e. set to 200, in order to obtain
#   a matrix with the desired characteristics.
#

for x in "$@" ; do
    if [[ $x =~ ^[0-9a-zA-Z_/.-]+=[0-9a-zA-Z_/.-]+$ ]] ; then
        eval $x
    fi
done

for f in CADO_BUILD MPI ; do
    if ! [ "${!f}" ] ; then
        echo "\$$f must be defined" >&2
        exit 1
    fi
done

for f in krylov ; do
    if ! [ -x "$CADO_BUILD/linalg/bwc/$f" ] ; then
        echo "missing binary $CADO_BUILD/linalg/bwc/$f ; compile it first" >&2
        exit 1
    fi
done

set -ex

: ${nrows=37000000}
: ${density=250}

export DISPLAY=         # safety precaution
mpi=(
$MPI/bin/mpiexec -n 4
    -machinefile $OAR_NODEFILE
    --map-by node
    --mca plm_rsh_agent oarsh
    --mca mtl ofi
    --mca mtl_ofi_prov psm2
    --mca btl '^openib'
)
kargs=(
    m=2 n=1
    prime=62310183390859392032917522304053295217410187325839402877409394441644833400594105427518019785136254373754932384219229310527432768985126965285945608842159143181423474202650807208215234033437849707623496592852091515256274797185686079514642651
    wdir=/tmp
    mpi=2x2 thr=16x4
    sequential_cache_build=16
    ys=0..1
    random_matrix=$nrows,density=$density,c=32
    cpubinding="Package=>1x2 L2Cache*16=>16x2 PU*2=>1x1"
    start=0 interval=128 end=128
)

"${mpi[@]}" $CADO_BUILD/linalg/bwc/krylov "${kargs[@]}"
