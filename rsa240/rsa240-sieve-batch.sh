#!/bin/bash

set -e

: ${DATA?missing}
: ${CADO_BUILD?missing}
: ${wdir="/tmp"}
: ${result_dir="$DATA"}

set +e

# batch that number of survivors per file, to be sent to finishbatch
# 16M survivors create a product tree 0.75 times the product tree of the primes
# 10M survivors create a product tree 0.48 times the product tree of the primes
filesize=10000000

# A qrange of 100000 should (easily) fit in 2 hours.
# Set default q0 and q1 outside the real range, for safety.
q0=2000000000
q1=2000010000

while [ $# -ne 0 ] ; do
    if [ $# -gt 1 ] && [[ $2 =~ ^[0-9]+$ ]] && [[ $1 =~ ^-(q[01])$ ]] ; then
        eval "${BASH_REMATCH[1]}=$2"
        shift 2
    else
        echo "unknown argument $1"
        exit 1
    fi
done

# With that number of finishbatch jobs, we have enough.
nb_finishbatch="6"

# Create all the required subdirectories
workdir="$wdir/wdir-${q0}-${q1}"
mkdir -p "$workdir"
mkdir -p "$workdir/surv"
mkdir -p "$workdir/running"

mkdir -p "$result_dir/rels"
resdir="$result_dir/rels/${q0}-${q1}"
mkdir -p "$resdir"
mkdir -p "$result_dir/log"


loop_finishbatch() {
    id=$1
    while true; do
        # try to get a file of survivors
        file=`ls -U $workdir/surv/ | grep -v ".part$" | sort -R | head -n 1`
        if [ -z $file ]; then
            # empty directory. Is las still running ?
            if [ -f "$workdir/las_done" ]; then
                return 0;
            fi
            # wait a bit and try again
            sleep $((RANDOM / 10000 + 1))
            continue;
        fi

        # mark the file as taken
        mv "$workdir/surv/$file" "$workdir/running/$file.$id" 2> /dev/null
        if [ $? != 0 ]; then
            # oops, someone took it before us !
            sleep $((RANDOM / 10000 + 1))
            continue;
        fi

        # run finishbatch on it
        echo -n "[ $id ]: Starting finishbatch on $file.$id at "; date
       
        $CADO_BUILD/sieve/ecm/finishbatch -poly              \
          $DATA/rsa240.poly -lim0 0 -lim1 2100000000   \
          -lpb0 36 -lpb1 37 -batch0 $DATA/rsa240.batch0\
          -batchlpb0 31 -batchmfb0 72 -batchlpb1 30 -batchmfb1 74 -doecm\
          -ncurves 80 -t 8 -in "$workdir/running/$file.$id" \
          > "$resdir/$file"

        echo -n "[ $id ]: End finishbatch on $file.$id at "; date

        # clean-up

        rm "$workdir/running/$file.$id"
    done
}

rm -f "$workdir/las_done"

for i in `seq 1 $nb_finishbatch`; do
    echo -n "Starting finishbatch slave $i at "; date
    loop_finishbatch $i &
done

echo -n "Starting las at "; date

$CADO_BUILD/sieve/las                           \
  -poly $DATA/rsa240.poly     \
  -fb1  $DATA/rsa240.fb1.gz    \
  -fbc  $wdir/rsa240.fbc       \
  -lim0 0 -lim1 2100000000 -lpb0 36 -lpb1 37 -sqside 1 -A 32 \
  -mfb0 250 -mfb1 74 -lambda0 5.0 -lambda1 2.0               \
  -bkmult 1,1l:1.15,1s:1.5,2s:1.1 -bkthresh1 50000000        \
  -adjust-strategy 2 -t auto -memory-margin 80               \
  -batch-print-survivors-filesize $filesize                  \
  -q0 $q0 -q1 $q1                                            \
  -batch-print-survivors "$workdir/surv/surv.${q0}-${q1}"  \
  -v  \
>  "$result_dir/log/las.${q0}-${q1}.out"
2> "$result_dir/log/las.${q0}-${q1}.err"

echo -n "End las at "; date

touch "$workdir/las_done"

wait

echo -n "End of computation at "; date

