# Additional info on filtering for RSA-240

Filtering was run exclusively on the machine `wurst`.

A first step of the filtering process in cado-nfs is to create the
so-called "renumber table", as follows.
```
$CADO_BUILD/sieve/freerel -poly rsa240.poly -renumber $DATA/rsa240.renumber.gz -lpb0 36 -lpb1 37 -out $DATA/rsa240.freerel -t 32
```
where `-t 32` specifies the number of threads. This was done with revision
`30a5f3eae` of cado-nfs, and takes several hours. (Note that newer
versions of cado-nfs changed the format of this file.)

## Duplicate removal

Duplicate removal was done with revision `50ad0f1fd` of cado-nfs.
Cado-nfs proceeds through two passes. We used the default cado-nfs
setting which, on the first pass, splits the input into `2^2=4`
independent slices, with no overlap.  Cado-nfs supports doing this step
in an incremental way, so that we assume below the shell variable
`EXP` expands to an integer indicating the filtering experiment number.
In the command below, `$new_files` is expected to expand to a file
containing a list of file names of new relations (relative to `$DATA`) to
add to the stored set of relations.
```
mkdir -p $DATA/dedup/{0..3}
$CADO_BUILD/filter/dup1 -prefix dedup -basepath $DATA -filelist $new_files -out $DATA/dedup/ -n 2 > $DATA/dup1.$EXP.stdout 2> $DATA/dup1.$EXP.stderr
grep '^# slice.*received' $DATA/dup1.$EXP.stderr > $DATA/dup1.$EXP.per_slice.txt
```

This first pass takes about 6 hours. Numbers of relations per slice are
printed by the program and must be saved for later use (hence the
`$DATA/dup1.$EXP.per_slice.txt` file).
The second pass of duplicate removal works independently on each of the
non-overlapping slices. The number of slices can thus be used as a sort
of time-memory tradeoff. 
```
for i in {0..3} ; do
    nrels=`awk '/slice '$i' received/ { x+=$5 } END { print x; }' $DATA/dup1.*.per_slice.txt`
    $CADO_BUILD/filter/dup2 -nrels $nrels -renumber $DATA/rsa240.renumber.gz $DATA/dedup/$i/dedup*gz > $DATA/dup2.$EXP.$i.stdout 2> $DATA/dup2.$EXP.$i.stderr
done
```
(Note: in newer versions of cado-nfs, after June 2020, the `dup2`
programs also requires the arguments `-poly rsa240.poly`.)

## The "purge" step, a.k.a. singleton and "clique" removal.

This step was done with revision `50ad0f1fd` of cado-nfs. We assume below
that `$EXP` is consistent with the latest pass of duplicate removal that
was done following the steps above.
```
nrels=$(awk  '/remaining/ { x+=$4; } END { print x }' $DATA/dup2.$EXP.[0-3].stderr)
colmax=8460702956
$CADO_BUILD/filter/purge -out purged$EXP.gz -nrels $nrels -keep 160 -col-min-index 0 -col-max-index $colmax -t 56 -required_excess 0.0 $DATA/dedup/*/dedup*gz
```

An excerpt of the output is:
```
...
Step 0: only singleton removal
Sing. rem.: begin with: nrows=6011911051 ncols=6334109673 excess=-322198622 at 23480.64
...
Sing. rem.:   iter 022: nrows=2603459110 ncols=2383461671 excess=219997439 at 95962.02
Sing. rem.:   iter 023: No more singletons, finished at 98209.91
...
Step 1 of 10: target excess is 197997712
...
Sing. rem.: begin with: nrows=2377125995 ncols=2179128283 excess=197997712 at 111193.00
...
Sing. rem.:   iter 012: nrows=2361173984 ncols=2163157921 excess=198016063 at 136941.57
Sing. rem.:   iter 013: No more singletons, finished at 139133.50
...
Step 10 of 10: target excess is 59844
...
Sing. rem.: begin with: nrows=1182977166 ncols=1182917322 excess=59844 at 344094.50
...
Sing. rem.:   iter 008: nrows=1176112660 ncols=1176045092 excess=67568 at 352736.86
Sing. rem.:   iter 009: No more singletons, finished at 353818.33

Step extra: target excess is 160
...
Sing. rem.: begin with: nrows=1175353550 ncols=1175353390 excess=160 at 358624.22
...
Sing. rem.:   iter 004: nrows=1175353278 ncols=1175353118 excess=160 at 362782.32
Sing. rem.:   iter 005: No more singletons, finished at 363841.06
...
Final values:
nrows=1175353278 ncols=1175353118 excess=160
weight=27090768157 weight*nrows=3.18e+19
Total usage: time 379175s (cpu), 36492s (wct) ; memory 3675M, peak 1314500M
```

## The "merge" step

The merge step used revision `8e651c0a6` of cado-nfs, which implements the
algorithm described in the article

Charles Bouillaguet and Paul Zimmermann, _Parallel Structured Gaussian Elimination for the Number Field Sieve_, April 2019, [`https://hal.inria.fr/hal-02098114`](https://hal.inria.fr/hal-02098114)

```
$CADO_BUILD/filter/merge -out $DATA/history$EXP -t 56 -target_density 200 -mat $DATA/purged$EXP.gz -skip 32
```

An excerpt of the output is:
```
...
# Done: Read 1175353278 relations in 2444.8s -- 69.6 MB/s -- 480749.2 rels/s
Time for filter_matrix_read: 4786.54s
exit renumber, ncols=1175353086
   renumber took 2326.0s (cpu), 65.9s (wct) [cpu/wct=35.3]
Using MERGE_LEVEL_MAX=32, CBOUND_INCR=13, OpenMP 201511
N=1175353278 W=22679059210 W/N=19.30 cpu=7112.6s wct=2511.9s mem=337451M
   compute_weights took 1729.9s (cpu), 40.4s (wct) [cpu/wct=42.8]
   compute_R took 739.9s (cpu), 20.8s (wct) [cpu/wct=35.6]
   compute_merges took 359.9s (cpu), 7.0s (wct) [cpu/wct=51.1]
   apply_merges took 3209.9s (cpu), 1045.2s (wct) [cpu/wct=3.1]
   buffer_flush took 1.3s (cpu), 8.4s (wct) [cpu/wct=0.2]
   pass took 6040.9s (cpu), 1124.2s (wct) [cpu/wct=5.4]
...
N=282336644 W=56537906027 (433504MB) W/N=200.25 fill-in=412.63 cpu=131055.8s wct=8832.5s mem=795576M [pass=55,cwmax=32]
Estimated N=282452311 for W/N=200.00
renumber       : 2326.0s (cpu), 65.9s (wct) [cpu/wct=35.3]
compute_weights: 1729.9s (cpu), 40.4s (wct) [cpu/wct=42.8]
compute_R      : 49402.5s (cpu), 1679.5s (wct) [cpu/wct=29.4]
compute_merges : 21854.0s (cpu), 395.3s (wct) [cpu/wct=55.3]
apply_merges   : 47457.5s (cpu), 3887.8s (wct) [cpu/wct=12.2]
pass           : 123943.3s (cpu), 6320.6s (wct) [cpu/wct=19.6]
recompress     : 3488.0s (cpu), 67.0s (wct) [cpu/wct=52.0]
buffer_flush   : 11.4s (cpu), 60.7s (wct) [cpu/wct=0.2]
Final matrix has N=282336644 nc=282336452 (192) W=56537906027
Before cleaning memory:
Total usage: time 126269s (cpu), 6386s (wct) ; memory 703913M, peak 795576M
After cleaning memory:
Total usage: time 126883s (cpu), 6480s (wct) ; memory 536456M, peak 795576M
Total usage: time 131669s (cpu), 8926s (wct) ; memory 536456M, peak 795576M
```

## The "replay" step

The replay step used revision `1369194a5` of cado-nfs.

```
$CADO_BUILD/replay -purged $DATA/purged$EXP.gz -his $DATA/history$EXP -out $DATA/rsa240.matrix$EXP.200.bin -index $DATA/rsa240.index$EXP.gz
```

An excerpt of the output is:
```
...
# Done: Read 1175353278 relations in 2635.0s -- 64.6 MB/s -- 446058.1 rels/s
The biggest index appearing in a relation is 8460702945
Reading row additions
...
# Done: Read 2516540706 row additions in 15177.9s -- 165802.9 lines/s
Renumbering columns (including sorting w.r.t. weight)
Sorting 282336484 columns by decreasing weight
Sparse submatrix: nrows=282336644 ncols=282336484
...
# Writing matrix took 10146.6s
# Weight of the sparse submatrix: 56524556188
Total usage: time 32650s (cpu), 32502s (wct) ; memory 802686M, peak 915127M
```

